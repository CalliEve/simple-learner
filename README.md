# Simple Learner

This repo contains the following machine learning algorithms:

- Simple evolving neural network in `src/evolving`
- KMeans clustering algorithm in `src/kmeans`
- Simple Linear Regression algorithm in `src/simple_linear_regression`
- Multiple Linear Regression algorithm in `src/multiple_linear_regression`
- Logistic Regression algorithm in `src/logistic_regression`
- Decision Tree algorithm in `src/decision_tree` 
- Least Squares algorithm in `src/least_squares`
- K Nearest Neigbors in `src/k_nearest_neigbors`
- Local Outlier Factor in `src/local_outlier_factor`

## Examples

In addition this repo also contains at least one example for each in `examples/`. These can be ran by running `cargo run` in their directories.

Note that when running some examples like `examples/slide_blocks_game`, it is advised to use the release flag to make it go fast enough.
