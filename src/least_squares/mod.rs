use ndarray::{
    stack,
    Array1,
    Array2,
    ArrayView1,
    Axis,
};

use crate::{
    utils::calculations::rref,
    DataValue,
    Error,
};

#[derive(Debug, Clone, Default)]
pub struct LeastSquares {
    fitted: Vec<f64>,
    max_degree: usize,
    degree: usize,
}

impl LeastSquares {
    pub fn new() -> Self {
        Self {
            fitted: Vec::new(),
            max_degree: 25,
            degree: 0,
        }
    }

    pub fn set_max_degree(&mut self, max_degree: usize) -> &mut Self {
        self.max_degree = max_degree;
        self
    }

    pub fn fit(
        &mut self,
        data_column: ArrayView1<DataValue>,
        res_column: ArrayView1<DataValue>,
    ) -> Result<(), Error> {
        if !data_column
            .iter()
            .all(|v| v.is_numeric())
            || !res_column
                .iter()
                .all(|v| v.is_numeric())
        {
            return Err(Error::NotNumeric);
        }
        let data_column_floats = data_column.map(|v| {
            v.as_float()
                .unwrap()
        });
        let res_column_floats = res_column.map(|v| {
            v.as_float()
                .unwrap()
        });

        let mut degree = 1;
        let mut iterations_since_best = 0;
        let mut lowest_error = f64::INFINITY;
        let mut best_fit = Vec::new();

        loop {
            let data = pad_matrix(data_column_floats.view(), degree);

            let transposed = data.t();

            let atd = transposed.dot(&Array2::from_shape_vec(
                (data.nrows(), 1),
                res_column_floats.to_vec(),
            )?);
            let mut augmented_matrix = transposed.dot(&data);
            augmented_matrix.push_column(atd.column(0))?;

            let rref_res = rref(augmented_matrix);

            let trained = Self {
                fitted: rref_res
                    .columns()
                    .into_iter()
                    .last()
                    .unwrap()
                    .to_vec(),
                max_degree: 0,
                degree: 0,
            };

            let error = trained
                .total_error(data_column, res_column)?
                .as_float()
                .ok_or(Error::NotNumeric)?;

            if lowest_error > error {
                lowest_error = error;
                iterations_since_best = 0;
                best_fit = trained.fitted;
                dbg!((degree, lowest_error));
            }

            if iterations_since_best > 2 || degree >= self.max_degree {
                break;
            }

            iterations_since_best += 1;
            degree += 1
        }

        self.fitted = best_fit;
        self.degree = degree;

        Ok(())
    }

    pub fn guess(&self, x: &DataValue) -> Result<DataValue, Error> {
        self.guess_float(
            x.as_float()
                .ok_or(Error::NotNumeric)?,
        )
        .map(DataValue::Float)
    }

    pub fn guess_float(&self, x: f64) -> Result<f64, Error> {
        if self
            .fitted
            .is_empty()
        {
            return Err(Error::NotFitted);
        }

        let dim = self
            .fitted
            .len();
        let mut res = 0f64;

        for i in 0..dim {
            res += self.fitted[i] * x.powi(i as i32)
        }

        Ok(res)
    }

    pub fn standard_deviation(
        &self,
        data_column: ArrayView1<DataValue>,
        res_column: ArrayView1<DataValue>,
    ) -> Result<f64, Error> {
        let mut deviations = Vec::new();

        for (i, val) in data_column
            .into_iter()
            .enumerate()
        {
            let x = val
                .as_float()
                .ok_or(Error::NotNumeric)?;
            let y = res_column
                .get(i)
                .ok_or(Error::WrongDimensions)?
                .as_float()
                .ok_or(Error::NotNumeric)?;
            deviations.push(self.guess_float(x)? - y);
        }

        let mean: f64 = deviations
            .iter()
            .sum::<f64>()
            / deviations.len() as f64;
        Ok((deviations
            .iter()
            .map(|v| (v - mean).powi(2))
            .sum::<f64>()
            / (deviations.len() - 1) as f64)
            .sqrt())
    }

    pub fn distance_from_line(&self, x: &DataValue, y: &DataValue) -> Result<DataValue, Error> {
        let guess = self.guess(x)?;

        Ok(guess
            .max(y)?
            .sub(&guess.min(y)?))
    }

    pub fn total_error(
        &self,
        x_axis: ArrayView1<DataValue>,
        y_axis: ArrayView1<DataValue>,
    ) -> Result<DataValue, Error> {
        Ok(x_axis
            .into_iter()
            .enumerate()
            .map(|(i, x)| self.distance_from_line(x, &y_axis[i]))
            .collect::<Result<Vec<DataValue>, Error>>()?
            .into_iter()
            .map(|e| e.pow(2))
            .sum())
    }
}

fn pad_matrix(data_column: ArrayView1<f64>, degree: usize) -> Array2<f64> {
    let rows = data_column
        .into_iter()
        .map(|x| {
            (0..=degree)
                .into_iter()
                .map(|d| x.powi(d as i32))
                .collect::<Array1<f64>>()
        })
        .collect::<Vec<_>>();

    stack(
        Axis(0),
        rows.iter()
            .map(|a| a.view())
            .collect::<Vec<_>>()
            .as_slice(),
    )
    .unwrap()
}
