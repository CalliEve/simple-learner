use std::{
    collections::HashMap,
    fmt::Debug,
    hash::Hash,
};

use crate::{
    utils::calculations::euclidian_distance,
    Data,
    DataValue,
    Error,
};

#[derive(Clone)]
pub struct KNearestNeighbors<T: Clone + Eq + Hash> {
    inner: Vec<(Vec<f64>, T)>,
    k: usize,
    fitted: bool,
}

impl<T: Clone + Eq + Hash + Debug> KNearestNeighbors<T> {
    pub fn new(k: usize) -> Self {
        Self {
            fitted: false,
            inner: Vec::new(),
            k,
        }
    }

    pub fn set_k(&mut self, k: usize) -> &mut Self {
        self.k = k;
        self.fitted = false;
        self
    }

    pub fn fit(&mut self, rows: &[(Vec<f64>, T)]) -> &mut Self {
        self.inner = rows.to_vec();
        self.fitted = true;

        self
    }

    pub fn guess_floats(&self, row: &[f64]) -> Result<T, Error> {
        if !self.fitted {
            return Err(Error::NotFitted);
        }

        self.calc_category(row)
    }

    pub fn guess_and_add_floats(&mut self, row: &[f64]) -> Result<T, Error> {
        if !self.fitted {
            return Err(Error::NotFitted);
        }

        self.calc_category_and_add(row)
    }

    fn calc_category_and_add(&mut self, row: &[f64]) -> Result<T, Error> {
        let cat = self.calc_category(row)?;

        self.inner
            .push((row.to_vec(), cat.clone()));

        Ok(cat)
    }

    fn calc_category(&self, row: &[f64]) -> Result<T, Error> {
        match self
            .inner
            .first()
        {
            Some(r)
                if r.0
                    .len()
                    != row.len() =>
            {
                return Err(Error::WrongDimensions)
            },
            _ => {},
        }

        self.get_nearest_neighbors(row)
            .into_iter()
            .fold(HashMap::with_capacity(5), |mut acc, (d, _, c)| {
                if let Some((count, distance)) = acc.get_mut(&c) {
                    *count += 1;
                    *distance += d;
                } else {
                    acc.insert(c, (1, d));
                }

                acc
            })
            .into_iter()
            .fold(None, |acc, (c, (count, dist))| {
                if let Some((mc, (m_count, m_dist))) = acc {
                    if count > m_count || (count == m_count && dist < m_dist) {
                        Some((c, (count, dist)))
                    } else {
                        Some((mc, (m_count, m_dist)))
                    }
                } else {
                    Some((c, (count, dist)))
                }
            })
            .map(|(c, _)| c)
            .ok_or(Error::NoData)
    }

    fn get_nearest_neighbors(&self, row: &[f64]) -> Vec<(f64, Vec<f64>, T)> {
        self.inner
            .iter()
            .map(|(r, c)| (euclidian_distance(r, row), r, c))
            .fold(Vec::with_capacity(5), |mut acc, mut d| {
                if acc.len() < 5 {
                    acc.push(d);
                } else {
                    for r in acc.iter_mut() {
                        if r.0 > d.0 {
                            std::mem::swap(&mut (*r), &mut d)
                        }
                    }
                }
                acc
            })
            .into_iter()
            .map(|(d, r, c)| (d, r.clone(), c.clone()))
            .collect()
    }
}

impl KNearestNeighbors<HashableDataValue> {
    pub fn from_data(
        k: usize,
        data: Data,
        res_column: &str,
    ) -> Result<KNearestNeighbors<HashableDataValue>, Error> {
        let data_columns = data
            .iter_columns()
            .filter(|(c, _)| *c != res_column)
            .map(|(_, d)| {
                d.into_iter()
                    .map(|v| {
                        v.as_float()
                            .ok_or(Error::NotNumeric)
                    })
                    .collect::<Result<Vec<f64>, Error>>()
            })
            .collect::<Result<Vec<Vec<f64>>, Error>>()?
            .into_iter()
            .fold(Vec::<Vec<f64>>::new(), |mut acc, col| {
                for (i, val) in col
                    .into_iter()
                    .enumerate()
                {
                    if let Some(r) = acc.get_mut(i) {
                        r.push(val)
                    } else {
                        acc.push(vec![val])
                    }
                }

                acc
            })
            .into_iter()
            .collect::<Data>();

        let inner = data
            .get_column(res_column)
            .ok_or(Error::NoData)?
            .1
            .into_iter()
            .zip(data_columns.iter_rows())
            .map(|(res, row)| {
                (
                    row.map(|v| {
                        v.as_float()
                            .unwrap()
                    })
                    .to_vec(),
                    HashableDataValue::from(res.clone()),
                )
            })
            .collect::<Vec<(Vec<f64>, _)>>();

        Ok(Self {
            inner,
            k,
            fitted: true,
        })
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum HashableDataValue {
    String(String),
    Bool(bool),
    Integer(i32),
    Null,
}

impl HashableDataValue {
    pub fn as_int(self) -> Option<i32> {
        match self {
            Self::Integer(i) => Some(i),
            Self::Null => Some(0),
            _ => None,
        }
    }

    pub fn as_bool(self) -> Option<bool> {
        match self {
            Self::Bool(b) => Some(b),
            _ => None,
        }
    }

    pub fn as_string(self) -> String {
        match self {
            Self::String(s) => s,
            Self::Integer(i) => i.to_string(),
            Self::Bool(b) => b.to_string(),
            Self::Null => "null".to_string(),
        }
    }
}

impl From<DataValue> for HashableDataValue {
    fn from(value: DataValue) -> Self {
        match value {
            DataValue::String(s) => HashableDataValue::String(s),
            DataValue::Bool(b) => HashableDataValue::Bool(b),
            DataValue::Null => HashableDataValue::Null,
            DataValue::Integer(i) => HashableDataValue::Integer(i),
            DataValue::Float(f) => HashableDataValue::String(f.to_string()),
        }
    }
}
