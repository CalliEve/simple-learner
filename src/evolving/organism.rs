use rand::prelude::RngCore;
use rand_distr::Distribution;

use super::{
    logic::Logic,
    ActivationFormula,
    Learnable,
};

/// This struct manages the connection between a [`Learnable`] and its [`Logic`]
/// struct, keeping track of its score and if it has finished.
#[derive(Clone, Debug)]
pub struct Organism<L>
where
    L: Learnable + Sized + Send,
{
    pub(super) to_learn: L,
    pub(super) finished: bool,
    pub(super) logic: Logic,
    pub(super) activation_formula: ActivationFormula,
}

impl<L> Organism<L>
where
    L: Learnable + Sized + Send,
{
    /// Creates a new Organism based on the given [`Learnable`] and constructs a
    /// new [`Logic`] core based on the given layers dimensions and the rng.
    pub fn new(
        to_learn: L,
        dimensions: &[usize],
        activation_formula: ActivationFormula,
        rng: &mut impl RngCore,
    ) -> Self {
        Self {
            finished: false,
            logic: Logic::new(rng, dimensions),
            to_learn,
            activation_formula,
        }
    }

    /// Returns if the Organism has finished.
    pub fn has_finished(&self) -> bool {
        self.finished
    }

    /// Returns the current score in the [`Learnable`].
    pub fn score(&self) -> i32 {
        self.to_learn
            .get_points()
    }

    /// Resets everything except the logic component of the Organism.
    pub fn reset(&mut self) {
        self.to_learn
            .reset();
        self.finished = false;
    }

    /// Makes a guess based on the [`Learnable::get_input_data`] and gives it to
    /// the [`Learnable::give_guess`], setting finished to true if it
    /// returns an error.
    pub fn make_guess(&mut self) {
        let guess = self
            .logic
            .calculate(
                self.to_learn
                    .get_input_data(),
                self.activation_formula,
            );

        let res = self
            .to_learn
            .give_guess(guess);

        if res.is_err() {
            self.finished = true;
        }
    }

    /// Mutate the logic core of the organism.
    ///
    /// This is done by adding a random number to each weight and bias according
    /// to a normal distribution of 0.0-0.09
    pub fn mutate(&mut self, rng: &mut impl RngCore) {
        let normal = rand_distr::Normal::new(0.0, 0.03).unwrap();

        self.logic
            .weights
            .iter_mut()
            .for_each(|ws| ws.mapv_inplace(|f| f + normal.sample(rng)));
        self.logic
            .biases
            .iter_mut()
            .for_each(|ws| ws.mapv_inplace(|f| f + normal.sample(rng)));
    }
}

impl<L> PartialEq for Organism<L>
where
    L: Learnable + Sized + Send,
{
    fn eq(&self, other: &Self) -> bool {
        self.logic == other.logic
    }
}
