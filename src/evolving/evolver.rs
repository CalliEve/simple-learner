use std::sync::Arc;

use crossbeam::sync::WaitGroup;
use parking_lot::Mutex;
use rand::prelude::*;
use threadpool::ThreadPool;

use super::{
    logic::Logic,
    organism::Organism,
    ActivationFormula,
    Learnable,
};

/// The Struct that manages the organisms and their evolutions over the
/// generations that the program is ran for.
#[derive(Clone, Debug)]
pub struct Evolver<L>
where
    L: Learnable + Sized + Send,
{
    threads: ThreadPool,
    organisms: Vec<Organism<L>>,
    rng: ThreadRng,
    best: i32,
    print_to_shell: bool,
    current_generation: usize,
}

impl<L> Evolver<L>
where
    L: Learnable + Sized + Send + 'static,
{
    /// Creates a builder to make an Evolver with.
    pub fn builder() -> EvolverBuilder<L> {
        EvolverBuilder::new()
    }

    /// Handles the evolution of the organisms.
    ///
    /// This is done in the following ways:
    /// - organisms get sorted
    /// - top 10% gets selected and get mated with, according to an exponential
    ///   distribution, randomly selected organisms
    /// - the children of these matings undergo mutation
    /// - the best of the past round gets added back to the new organism list
    pub fn evolve(&mut self) {
        self.organisms
            .sort_unstable_by_key(|x| x.score());
        self.organisms
            .reverse();

        let hold_out = self.organisms[..(self
            .organisms
            .len()
            / 10)]
            .to_vec();

        let exp = rand_distr::Exp::new(
            self.organisms
                .len() as f64,
        )
        .unwrap();
        let mut new_organisms = Vec::with_capacity(
            self.organisms
                .len(),
        );

        for i in 0..self
            .organisms
            .len()
        {
            let parent1 = hold_out
                .get(i % hold_out.len())
                .unwrap();
            let parent2 = self
                .organisms
                .get(exp.sample(&mut self.rng) as usize)
                .cloned()
                .unwrap();
            let mut child = self.mate(parent1, &parent2);

            child.mutate(&mut self.rng);

            new_organisms.push(child)
        }

        new_organisms[0] = self.organisms[0].clone();
        new_organisms[0].reset();

        self.organisms = new_organisms;
    }

    /// Run the evolver for one generation with the oragnisms spread out over
    /// the threadpool, note that this does not handle the evolving of the
    /// organisms.
    pub fn run_generation(&mut self) {
        let organisms = Arc::new(Mutex::new(Vec::with_capacity(
            self.organisms
                .len(),
        )));
        let wg = WaitGroup::new();

        for mut organism in self
            .organisms
            .clone()
            .into_iter()
        {
            let wg = wg.clone();
            let organisms = organisms.clone();

            self.threads
                .execute(move || {
                    while !organism.has_finished() {
                        organism.make_guess()
                    }

                    organisms
                        .lock()
                        .push(organism);

                    drop(organisms);
                    drop(wg);
                });
        }

        wg.wait();

        self.organisms = Arc::try_unwrap(organisms)
            .unwrap()
            .into_inner();
        self.current_generation += 1;
    }

    /// Run the evolver for N generations
    ///
    /// This is done by calling [`Self::run_generation`] and
    /// then [`Self::evolve`] N times in a loop, with printing an update if the
    /// point total of the best oragism changes.
    pub fn run_n_generations(&mut self, n: usize) {
        let n = self.current_generation + n;

        while self.current_generation <= n {
            self.run_generation();

            let winner = self
                .get_best()
                .clone();

            let print = winner
                .to_learn
                .print(self.current_generation);

            match winner.score() {
                x if x > self.best => {
                    self.best = winner.score();

                    if self.print_to_shell {
                        println!(
                            "\nEvolved on round {}: \nEvolution Points: {}\n{:?}",
                            self.current_generation,
                            winner.score(),
                            print,
                        );
                    }
                },
                x if x < self.best => {
                    panic!(
                        "organisms devolved in round {}, went from {} to {}",
                        self.current_generation,
                        self.best,
                        winner.score()
                    );
                },
                _ => {},
            }

            if self.current_generation % 50 == 0 {
                // println!(
                //     "\nWinner of round {}: \nEvolution Points: {}\n{:?}",
                //     self.current_generation,
                //     winner.final_score(),
                //     print,
                // );
            }

            self.evolve();
        }
    }

    /// Mate two oragnisms together
    ///
    /// This is done by setting all contents of the new organism to their
    /// defaults and then creating a new [`Logic`] struct by randomly
    /// choosing a bias or weight layer from one of the parents till all the
    /// needed layers having been gathered.
    fn mate(&mut self, organism1: &Organism<L>, organism2: &Organism<L>) -> Organism<L> {
        let mut to_learn = organism1
            .to_learn
            .clone();

        to_learn.reset();

        Organism {
            finished: false,
            to_learn,
            activation_formula: organism1.activation_formula,
            logic: Logic {
                biases: organism1
                    .logic
                    .biases
                    .iter()
                    .zip(
                        &organism2
                            .logic
                            .biases,
                    )
                    .map(|(p1, p2)| {
                        *[p1, p2]
                            .choose(&mut self.rng)
                            .unwrap()
                    })
                    .cloned()
                    .collect(),
                weights: organism1
                    .logic
                    .weights
                    .iter()
                    .zip(
                        &organism2
                            .logic
                            .weights,
                    )
                    .map(|(p1, p2)| {
                        *[p1, p2]
                            .choose(&mut self.rng)
                            .unwrap()
                    })
                    .cloned()
                    .collect(),
            },
        }
    }

    /// Gets the best organism
    fn get_best(&mut self) -> &Organism<L> {
        self.organisms
            .sort_unstable_by_key(|x| x.score());
        self.organisms
            .last()
            .unwrap()
    }
}

/// A helper struct for building an [`Evolver`].
pub struct EvolverBuilder<L>
where
    L: Learnable + Sized + Send,
{
    to_learn: Option<L>,
    thread_count: usize,
    to_learn_creator: Option<Arc<dyn Fn() -> L>>,
    organism_count: usize,
    layer_dimensions: Vec<usize>,
    activation_formula: ActivationFormula,
    print_to_shell: bool,
}

impl<L> EvolverBuilder<L>
where
    L: Learnable + Sized + Send,
{
    /// Create a new builder
    pub fn new() -> Self {
        Self {
            to_learn: None,
            thread_count: 8,
            to_learn_creator: None,
            organism_count: 100,
            layer_dimensions: vec![25, 50, 50, 5],
            activation_formula: ActivationFormula::Linear,
            print_to_shell: true,
        }
    }

    /// Sets the object to train the neural network against to the given object
    pub fn set_to_learn(&mut self, to_learn: L) -> &mut Self {
        self.to_learn = Some(to_learn);
        self
    }

    /// Sets what function to call to create the `to_learn` object with. If this
    /// isn't set it defaults to whatever is set with
    /// [`Self::set_to_learn`].
    pub fn set_to_learn_creator(&mut self, func: impl Fn() -> L + 'static) -> &mut Self {
        self.to_learn_creator = Some(Arc::new(Box::new(func)));
        self
    }

    /// Sets the amount of threads to use when running a generation against the
    /// [`Learnable`] object. Defaults to 8.
    pub fn set_thread_count(&mut self, count: usize) -> &mut Self {
        self.thread_count = count;
        self
    }

    /// Sets the amount of organisms to use. Defaults to 100.
    pub fn set_organism_count(&mut self, count: usize) -> &mut Self {
        self.organism_count = count;
        self
    }

    /// Sets the dimension of the layers of the neural network. Defaults to [25,
    /// 50, 50, 5]
    pub fn set_layer_dimensions(&mut self, dimensions: Vec<usize>) -> &mut Self {
        self.layer_dimensions = dimensions;
        self
    }

    /// Sets the activation formula for use by the logic core. Defaults to
    /// Linear
    pub fn set_activation_formula(&mut self, activation_formula: ActivationFormula) -> &mut Self {
        self.activation_formula = activation_formula;
        self
    }

    /// Sets if after every improvement, the current best result should be
    /// printed to the shell, though the print function will always be ran.
    /// Defaults to true.
    pub fn set_print_to_shell(&mut self, b: bool) -> &mut Self {
        self.print_to_shell = b;
        self
    }

    /// Builds the [`Evolver`] from the contents of this builder.
    ///
    /// **Warning:** panics when neither `to_learn` nor `to_learn_creator` have
    /// been set.
    pub fn build(&mut self) -> Evolver<L> {
        if self
            .to_learn
            .is_none()
            && self
                .to_learn_creator
                .is_none()
        {
            panic!("Neither to_learn nor to_learn_creator has been set for the EvolverBuilder");
        }

        let mut rng = thread_rng();

        let mut organisms = Vec::with_capacity(self.organism_count);
        organisms.resize_with(self.organism_count, || {
            self.to_learn_creator
                .clone()
                .map(|f| f())
                .or_else(|| {
                    self.to_learn
                        .clone()
                })
                .map(|l| {
                    Organism::new(l, &self.layer_dimensions, self.activation_formula, &mut rng)
                })
                .unwrap()
        });

        Evolver {
            threads: ThreadPool::new(self.thread_count),
            organisms,
            rng,
            best: i32::MIN,
            print_to_shell: self.print_to_shell,
            current_generation: 0,
        }
    }
}

impl<L> Default for EvolverBuilder<L>
where
    L: Learnable + Sized + Send,
{
    fn default() -> Self {
        Self::new()
    }
}
