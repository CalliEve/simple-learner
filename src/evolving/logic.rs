use ndarray::prelude::*;
use rand::{
    prelude::RngCore,
    Rng,
};

#[derive(Debug, Clone, Copy)]
pub enum ActivationFormula {
    SoftMax,
    Sigmoid,
    Linear,
}

impl ActivationFormula {
    fn calculate_inplace(&self, arr: &mut Array2<f64>) {
        match *self {
            Self::SoftMax => {
                *arr = arr.mapv(|f| f.exp())
                    / arr
                        .mapv(|f| f.exp())
                        .sum_axis(Axis(1))
            },
            Self::Sigmoid => arr.mapv_inplace(|f| 1.0 / (1.0 + f.exp())),
            Self::Linear => {},
        }
    }
}

/// The logic core of an organism, this struct holds the weights and biases
/// layers of a neural network belonging to that organism.
#[derive(Clone, Debug, PartialEq)]
pub struct Logic {
    pub weights: Vec<Array2<f64>>,
    pub biases: Vec<Array2<f64>>,
}

impl Logic {
    /// Creates a new logic struct based on random values from the given rng and
    /// in the shape of the given dimensions.
    pub fn new(rng: &mut impl RngCore, dimensions: &[usize]) -> Self {
        let mut weights = Vec::new();
        let mut biases = Vec::new();

        for i in 0..(dimensions.len() - 1) {
            let shape = (dimensions[i], dimensions[i + 1]);
            let range_max = (2.0 / ((shape.0 + shape.1) as f64)).powi(2);
            weights.push(Array2::from_shape_simple_fn(shape, || {
                rng.gen_range(0.0..range_max)
            }));
            biases.push(Array2::from_shape_simple_fn((1, shape.1), || {
                rng.gen_range(0.0..range_max)
            }))
        }

        Self {
            weights,
            biases,
        }
    }

    /// Calculates an output integer based on the input values and the weights
    /// and biases held by the struct.
    pub fn calculate(&self, input: Vec<f64>, activation: ActivationFormula) -> (usize, f64) {
        let mut calc_array: Array2<f64> = Array2::from_shape_vec((1, input.len()), input).unwrap();

        for (i, (weights, bias)) in self
            .weights
            .iter()
            .zip(&self.biases)
            .enumerate()
        {
            calc_array =
                calc_array.dot(weights) + Array2::ones((calc_array.shape()[0], 1)).dot(bias);

            if i == self
                .weights
                .len()
                - 1
            {
                break;
            }

            for x in calc_array.iter_mut() {
                *x = if *x < 0.0 { 0.0 } else { *x };
            }
        }

        activation.calculate_inplace(&mut calc_array);

        calc_array
            .iter()
            .enumerate()
            .fold(
                (0_usize, f64::MIN),
                |acc, (i, n)| if acc.1 > *n { acc } else { (i, *n) },
            )
    }
}
