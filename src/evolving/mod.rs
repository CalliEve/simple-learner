use std::fmt::Debug;

mod evolver;
mod logic;
mod organism;

pub use evolver::{
    Evolver,
    EvolverBuilder,
};
pub use logic::ActivationFormula;

/// Describes an object that the [`Evolver`] can be used to train an evolving
/// neural network against.
pub trait Learnable: Clone + Debug {
    type Error;

    /// Gets the input data for a guess from an organism.
    fn get_input_data(&self) -> Vec<f64>;

    /// Returns the guess from an organism to the Learnable.
    fn give_guess(&mut self, guess: (usize, f64)) -> Result<(), Self::Error>;

    /// Gets the amount of points the organism currently has 'scored'.
    fn get_points(&self) -> i32;

    /// Resets the Learnable.
    fn reset(&mut self);

    /// Prints the learnable outcome of the best organism to the commandline
    /// when the organism improves. Defaults to debug printing the
    /// learnable.
    fn print(&self, _current_generation: usize) -> String {
        format!("{:?}", self)
    }
}
