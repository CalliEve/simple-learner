use std::iter::IntoIterator;

use rand::{
    thread_rng,
    Rng,
};

use crate::{
    Data,
    DataValue,
    Error,
};

/// One cluster with its containing points and it center point.
#[derive(Clone, Debug, PartialEq)]
struct Cluster {
    data: Vec<Vec<DataValue>>,
    point: Vec<DataValue>,
}

/// The struct that does the KMeans clustering.
#[derive(Clone, Debug, PartialEq)]
pub struct KMeans {
    clusters: Vec<Cluster>,
    dimensions: usize,
}

impl KMeans {
    pub fn new(data: Data, columns: &[&str], cluster_count: usize) -> Result<Self, Error> {
        let mut data_cols: Vec<Vec<DataValue>> = vec![Vec::new(); data.row_len()];

        for (col_name, col) in data.iter_columns() {
            if columns.contains(&col_name) {
                for (index, value) in col
                    .into_iter()
                    .enumerate()
                {
                    data_cols[index].push(value.clone())
                }
            }
        }

        Self::from_iterator(data_cols, cluster_count)
    }

    /// Creates a new KMeans struct from the provided iterator with data and
    /// cluster count.
    ///
    /// Returns an error if the data contains less points than the asked amount
    /// of clusters, if not all points have the same dimensions, or if any of
    /// the data isn't numeric.
    pub fn from_iterator<T, V, D>(data: T, mut cluster_count: usize) -> Result<Self, Error>
    where
        T: IntoIterator<Item = V>,
        V: IntoIterator<Item = D>,
        D: Into<DataValue>,
    {
        let points: Vec<Vec<DataValue>> = data
            .into_iter()
            .map(|iter| {
                iter.into_iter()
                    .map(|d| d.into())
                    .map(|d| {
                        if d.is_numeric() {
                            Ok(d)
                        } else {
                            Err(Error::NotNumeric)
                        }
                    })
                    .collect::<Result<Vec<DataValue>, Error>>()
            })
            .collect::<Result<Vec<Vec<DataValue>>, Error>>()?;

        if points.len() < cluster_count {
            return Err(Error::NoData);
        }

        let dimensions = points[0].len();
        if points
            .iter()
            .any(|p| p.len() != dimensions)
        {
            return Err(Error::WrongDimensions);
        }

        let max_min = points
            .iter()
            .fold(
                Ok((points[0].clone(), points[0].clone())),
                |acc: Result<(Vec<DataValue>, Vec<DataValue>), Error>, point| {
                    let (max, min) = acc?;
                    Ok((
                        max.into_iter()
                            .zip(point.iter())
                            .map(|(x, y)| x.max(y))
                            .collect::<Result<Vec<DataValue>, Error>>()?,
                        min.into_iter()
                            .zip(point.iter())
                            .map(|(x, y)| x.min(y))
                            .collect::<Result<Vec<DataValue>, Error>>()?,
                    ))
                },
            )?;

        let mut rng = thread_rng();
        let mut clusters = Vec::with_capacity(cluster_count);

        while cluster_count > 0 {
            clusters.push(Cluster {
                data: Vec::new(),
                point: max_min
                    .0
                    .iter()
                    .zip(
                        max_min
                            .1
                            .iter(),
                    )
                    .map(|(max, min)| {
                        rng.gen_range(
                            (min.as_float()
                                .unwrap())
                                ..(max
                                    .as_float()
                                    .unwrap()),
                        )
                    })
                    .map(DataValue::from)
                    .collect(),
            });
            cluster_count -= 1;
        }
        clusters[0].data = points;

        let mut kmeans = Self {
            clusters,
            dimensions,
        };

        kmeans.sort_points();
        Ok(kmeans)
    }

    /// Sorts the points over the clusters.
    fn sort_points(&mut self) {
        let mut sorted: Vec<Cluster> = self
            .clusters
            .iter()
            .map(|c| Cluster {
                data: Vec::new(),
                point: c
                    .point
                    .clone(),
            })
            .collect();

        for cluster in &self.clusters {
            for point in &cluster.data {
                let index = self.belongs_to_cluster(point);
                sorted[index]
                    .data
                    .push(point.clone());
            }
        }

        self.clusters = sorted;
    }

    /// Calculates the new center point for the clusters and the calls
    /// [`Self::sort_points`].
    fn iterate(&mut self) {
        for cluster in &mut self.clusters {
            cluster.point = cluster
                .data
                .iter()
                .fold(vec![Vec::new(); self.dimensions], |mut acc, p| {
                    for (i, v) in p
                        .iter()
                        .enumerate()
                    {
                        acc[i].push(v);
                    }
                    acc
                })
                .into_iter()
                .map(|col| {
                    let len = (col.len() as f64).into();
                    col.into_iter()
                        .cloned()
                        .sum::<DataValue>()
                        .div(&len)
                })
                .collect();
        }

        self.sort_points();
    }

    /// Iterate over the data N times.
    pub fn iterate_n_times(&mut self, n: usize) {
        let mut i = 0;
        while i < n {
            self.iterate();
            i += 1
        }
    }

    /// Iterate over the data till the cluster centers don't move anymore.
    pub fn iterate_till_done(&mut self) {
        let mut cluster_points: Vec<Vec<DataValue>> = self
            .clusters
            .iter()
            .map(|c| &c.point)
            .cloned()
            .collect();

        self.iterate();

        while cluster_points
            != self
                .clusters
                .iter()
                .map(|c| {
                    c.point
                        .as_slice()
                })
                .collect::<Vec<&[DataValue]>>()
        {
            cluster_points = self
                .clusters
                .iter()
                .map(|c| &c.point)
                .cloned()
                .collect();
            self.iterate();
        }
    }

    /// Returns the points sorted over the clusters.
    pub fn get_points(&self) -> Vec<&[Vec<DataValue>]> {
        self.clusters
            .iter()
            .map(|c| {
                c.data
                    .as_slice()
            })
            .collect()
    }

    /// Returns the locations of the cluster centers.
    pub fn get_cluster_centers(&self) -> Vec<&[DataValue]> {
        self.clusters
            .iter()
            .map(|c| {
                c.point
                    .as_slice()
            })
            .collect()
    }

    /// Returns the index of the cluster the given point belongs to.
    pub fn belongs_to_cluster(&self, point: &[DataValue]) -> usize {
        self.clusters
            .iter()
            .enumerate()
            .map(|(i, c)| (i, calc_pythagorean_distance(point, &c.point)))
            .fold((0, DataValue::from(f64::MAX)), |acc, (i, d)| {
                if d < acc.1 {
                    (i, d)
                } else {
                    acc
                }
            })
            .0
    }
}

/// Calculates the distance between two points according to the pythagorean
/// theorem.
fn calc_pythagorean_distance(point1: &[DataValue], point2: &[DataValue]) -> DataValue {
    point1
        .iter()
        .zip(point2.iter())
        .map(|(x, y)| {
            x.sub(y)
                .pow(2)
        })
        .sum::<DataValue>()
        .sqrt()
}
