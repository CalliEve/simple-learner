use crate::{
    Data,
    DataValue,
    Error,
};

#[derive(Debug, Clone)]
pub struct MultipleLinearRegression {
    column_count: usize,
    a: f64,
    b: f64,
    r: f64,
}

impl MultipleLinearRegression {
    pub fn new(data: Data, dep_column: impl ToString) -> Result<Self, Error> {
        let dep_column = dep_column.to_string();

        let dep_column_index = data
            .iter_column_names()
            .position(|c| c == dep_column)
            .ok_or(Error::NoData)?;

        let data_vec: Vec<Vec<f64>> = data
            .iter_rows()
            .map(|r| {
                if r.iter()
                    .all(|f| f.is_numeric())
                {
                    Ok(r)
                } else {
                    Err(Error::NotNumeric)
                }
            })
            .map(|row| {
                Ok(row?
                    .into_iter()
                    .map(|v| {
                        v.as_float()
                            .unwrap()
                    })
                    .collect())
            })
            .collect::<Result<Vec<Vec<f64>>, Error>>()?;
        if data_vec.len() < 2 {
            return Err(Error::NoData);
        }
        let column_count = data_vec[0].len() - 1;

        let (a, b, r) = Self::fit(data_vec, dep_column_index);

        Ok(Self {
            column_count,
            a,
            b,
            r,
        })
    }

    fn fit(mut data: Vec<Vec<f64>>, dep_column: usize) -> (f64, f64, f64) {
        for row in data.iter_mut() {
            let dep_col_content = row.remove(dep_column);
            row.insert(0, dep_col_content);
        }

        let averages: Vec<f64> = data
            .iter()
            .fold(vec![0.0; data[0].len()], |acc, rows| {
                acc.into_iter()
                    .zip(rows.iter())
                    .map(|(a, b)| a + b)
                    .collect()
            })
            .into_iter()
            .map(|s| (s / data.len() as f64))
            .collect();

        let b = data
            .iter()
            .map(|v| {
                (v[0] - averages[0])
                    * v.iter()
                        .skip(1)
                        .zip(
                            averages
                                .iter()
                                .skip(1),
                        )
                        .map(|(x, avg)| x - avg)
                        .sum::<f64>()
            })
            .sum::<f64>()
            / data
                .iter()
                .map(|v| {
                    v.iter()
                        .skip(1)
                        .zip(
                            averages
                                .iter()
                                .skip(1),
                        )
                        .map(|(x, avg)| (x - avg))
                        .sum::<f64>()
                        .powi(2)
                })
                .sum::<f64>();

        let a = averages[0]
            - averages
                .iter()
                .skip(1)
                .sum::<f64>()
                * b;

        let r = (b * data
            .iter()
            .map(|v| (v[0] - averages[0]).powi(2))
            .sum::<f64>())
        .sqrt();

        (a, b, r)
    }

    pub fn guess(&self, vars: &[DataValue]) -> Result<DataValue, Error> {
        if vars.len() != self.column_count {
            return Err(Error::WrongDimensions);
        } else if vars
            .iter()
            .any(|v| !v.is_numeric())
        {
            return Err(Error::NotNumeric);
        }

        Ok((vars
            .iter()
            .map(|v| {
                v.as_float()
                    .unwrap()
            })
            .map(|x| x * self.b)
            .sum::<f64>()
            + self.a)
            .into())
    }

    pub fn guess_float(&self, vars: &[f64]) -> Result<f64, Error> {
        if vars.len() != self.column_count {
            return Err(Error::WrongDimensions);
        }

        Ok(vars
            .iter()
            .map(|x| x * self.b)
            .sum::<f64>()
            + self.a)
    }

    pub fn get_correlation_coefficient(&self) -> DataValue {
        self.r
            .into()
    }
}
