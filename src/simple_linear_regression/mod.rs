use crate::{
    Data,
    Error,
};

#[derive(Debug, Clone, Copy)]
pub struct SimpleLinearRegression {
    a: f64,
    b: f64,
    r: f64,
}

impl SimpleLinearRegression {
    pub fn new(data: Data) -> Result<Self, Error> {
        let data_vec: Vec<(f64, f64)> = data
            .iter_rows()
            .map(|r| {
                if r.iter()
                    .all(|f| f.is_numeric())
                {
                    Ok(r)
                } else {
                    Err(Error::NotNumeric)
                }
            })
            .map(|row| {
                let r = row?;
                Ok((
                    r[0].as_float()
                        .unwrap(),
                    r[1].as_float()
                        .unwrap(),
                ))
            })
            .collect::<Result<Vec<(f64, f64)>, Error>>()?;
        if data_vec.is_empty() {
            return Err(Error::NoData);
        }

        let (a, b, r) = Self::fit(&data_vec);

        Ok(Self {
            a,
            b,
            r,
        })
    }

    pub fn from_floats(data: &[(f64, f64)]) -> Self {
        let (a, b, r) = Self::fit(data);

        Self {
            a,
            b,
            r,
        }
    }

    fn fit(data: &[(f64, f64)]) -> (f64, f64, f64) {
        let sums: (f64, f64) = data
            .iter()
            .fold((0.0, 0.0), |(acc_x, acc_y), (x, y)| (acc_x + x, acc_y + y));
        let x_avg = sums.0 / data.len() as f64;
        let y_avg = sums.1 / data.len() as f64;

        let b = data
            .iter()
            .map(|v| (v.0 - x_avg) * (v.1 - y_avg))
            .sum::<f64>()
            / data
                .iter()
                .map(|v| (v.0 - x_avg).powi(2))
                .sum::<f64>();

        let a = y_avg - (b * x_avg);

        let r = (b * data
            .iter()
            .map(|v| (v.1 - y_avg).powi(2))
            .sum::<f64>())
        .sqrt();

        (a, b, r)
    }

    pub fn guess(&self, x: f64) -> f64 {
        self.a + x * self.b
    }

    pub fn get_correlation_coefficient(&self) -> f64 {
        self.r
    }
}
