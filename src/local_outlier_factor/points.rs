#[derive(Debug, Clone, PartialEq)]
pub struct Point {
    coordinates: Vec<f64>,
    normalized: Vec<f64>,
    k_distance: f64,
    local_reachability_density: f64,
    local_outlier_factor: f64,
}

impl Point {
    pub fn new(coordinates: Vec<f64>, normalized: Vec<f64>) -> Self {
        Self {
            coordinates,
            normalized,
            k_distance: f64::INFINITY,
            local_reachability_density: f64::INFINITY,
            local_outlier_factor: f64::INFINITY,
        }
    }

    pub fn into_tuple(self) -> Option<(Vec<f64>, f64)> {
        (self.local_outlier_factor != f64::INFINITY)
            .then_some((self.coordinates, self.local_outlier_factor))
    }

    pub fn get_normalized(&self) -> &[f64] {
        &self.normalized
    }

    pub fn get_k_distance(&self) -> f64 {
        self.k_distance
    }

    pub fn get_local_reachability_density(&self) -> f64 {
        self.local_reachability_density
    }

    pub fn set_k_distance(mut self, k_distance: f64) -> Self {
        self.k_distance = k_distance;
        self
    }

    pub fn set_local_reachability_density(mut self, local_reachability_density: f64) -> Self {
        self.local_reachability_density = local_reachability_density;
        self
    }

    pub fn set_local_outlier_factor(mut self, local_outlier_factor: f64) -> Self {
        self.local_outlier_factor = local_outlier_factor;
        self
    }
}
