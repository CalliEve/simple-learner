mod points;

use points::Point;

use crate::{
    tools::euclidian_distance,
    utils::data_preparations::normalize_data,
    Data,
    DataValue,
    Error,
};

pub struct LocalOutlierFactor {
    k: usize,
    inner: Vec<(Vec<f64>, f64)>,
    std_dev: Option<f64>,
}

impl LocalOutlierFactor {
    pub fn new(k: usize) -> Self {
        Self {
            inner: Vec::new(),
            k,
            std_dev: None,
        }
    }

    pub fn set_k(&mut self, k: usize) -> &mut Self {
        self.k = k;
        self
    }

    pub fn fit(&mut self, data: Data, columns: &[&str]) -> Result<&mut Self, Error> {
        let column_indexes = columns
            .iter()
            .filter_map(|name| {
                data.iter_column_names()
                    .position(|c| &c == name)
            })
            .collect::<Vec<usize>>();
        if column_indexes.is_empty() {
            return Err(Error::NoData);
        }

        let rows = data
            .iter_rows()
            .map(|r| {
                r.into_iter()
                    .enumerate()
                    .filter_map(|(i, r)| {
                        if column_indexes.contains(&i) {
                            Some(r)
                        } else {
                            None
                        }
                    })
                    .map(|d| {
                        d.as_float()
                            .ok_or(Error::NotNumeric)
                    })
                    .collect::<Result<Vec<f64>, Error>>()
            })
            .collect::<Result<Vec<Vec<f64>>, Error>>()?;

        let points = normalize_data(data, columns)?
            .iter_rows()
            .map(|r| {
                r.into_iter()
                    .enumerate()
                    .filter_map(|(i, r)| {
                        if column_indexes.contains(&i) {
                            Some(r)
                        } else {
                            None
                        }
                    })
                    .map(|d| {
                        d.as_float()
                            .ok_or(Error::NotNumeric)
                    })
                    .collect::<Result<Vec<f64>, Error>>()
            })
            .collect::<Result<Vec<Vec<f64>>, Error>>()?
            .into_iter()
            .zip(rows)
            .map(|(normalized, coordinates)| Point::new(coordinates, normalized))
            .collect::<Vec<Point>>();

        let with_k_distance = points
            .clone()
            .into_iter()
            .map(|point| {
                let k_distance = self
                    .get_k_nearest_neighbors(&points, &point)
                    .into_iter()
                    .fold(f64::NEG_INFINITY, |max, (d, _)| max.max(d));
                point.set_k_distance(k_distance)
            })
            .collect::<Vec<Point>>();

        let with_lrd = with_k_distance
            .clone()
            .into_iter()
            .map(|point| {
                let reachabilities = self
                    .get_k_nearest_neighbors(&with_k_distance, &point)
                    .into_iter()
                    .map(|(_, neighbor)| {
                        neighbor
                            .get_k_distance()
                            .max(euclidian_distance(
                                point.get_normalized(),
                                neighbor.get_normalized(),
                            ))
                    })
                    .collect::<Vec<_>>();

                point.set_local_reachability_density(
                    1_f64
                        / (reachabilities
                            .iter()
                            .fold(0_f64, |acc, x| acc + x)
                            / (self.k as f64)),
                )
            })
            .collect::<Vec<Point>>();

        self.inner = with_lrd
            .clone()
            .into_iter()
            .map(|point| {
                let lrds = self
                    .get_k_nearest_neighbors(&with_lrd, &point)
                    .into_iter()
                    .map(|(_, p)| p.get_local_reachability_density())
                    .collect::<Vec<f64>>();

                let lof = point.get_local_reachability_density()
                    / (lrds
                        .iter()
                        .fold(0_f64, |acc, x| acc + x)
                        / (self.k as f64));

                point.set_local_outlier_factor(lof)
            })
            .map(Point::into_tuple)
            .collect::<Option<Vec<_>>>()
            .ok_or(Error::NotFitted)?;

        self.std_dev = None;

        Ok(self)
    }

    fn get_k_nearest_neighbors(&self, data: &[Point], point: &Point) -> Vec<(f64, Point)> {
        data.iter()
            .filter(|o| o.get_normalized() != point.get_normalized())
            .map(|o| {
                (
                    euclidian_distance(o.get_normalized(), point.get_normalized()),
                    o.clone(),
                )
            })
            .fold(Vec::with_capacity(self.k), |mut acc, mut d| {
                if acc.len() < self.k {
                    acc.push(d);
                } else {
                    for r in acc.iter_mut() {
                        if r.0 > d.0 {
                            std::mem::swap(&mut (*r), &mut d)
                        }
                    }
                }
                acc
            })
    }

    fn get_standard_deviation(&mut self) -> Result<f64, Error> {
        if self
            .inner
            .is_empty()
        {
            return Err(Error::NotFitted);
        }

        if let Some(sd) = self.std_dev {
            return Ok(sd);
        }

        let std_dev = self
            .inner
            .iter()
            .map(|p| vec![p.1])
            .collect::<Data>()
        .standard_deviation()
        .into_iter()
        .next()
        .map(|(_, d)| d)
        .ok_or(Error::NoData)?;
        self.std_dev = Some(std_dev);
        Ok(std_dev)
    }

    pub fn get_outliers(&mut self, std_dev_mult: f64) -> Result<Vec<Vec<DataValue>>, Error> {
        let std_dev = self.get_standard_deviation()? * std_dev_mult;

        let lof_sum: f64 = self
            .inner
            .iter()
            .map(|p| p.1)
            .sum();
        let avg = lof_sum
            / (self
                .inner
                .len() as f64);
        let avg_plus_std = avg + std_dev;
        let avg_min_std = avg - std_dev;

        Ok(self
            .inner
            .iter()
            .filter(|p| p.1 > avg_plus_std || p.1 < avg_min_std)
            .map(|p| {
                p.0.iter()
                    .copied()
                    .map(DataValue::from)
                    .collect::<Vec<DataValue>>()
            })
            .collect())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_k_nearest_neighbors() {
        let point = Point::new(Vec::new(), vec![1.0, 1.0]);

        let points = vec![
            Point::new(Vec::new(), vec![1.0, 1.0]),
            Point::new(Vec::new(), vec![1.5, 0.5]),
            Point::new(Vec::new(), vec![4.0, 3.0]),
            Point::new(Vec::new(), vec![2.0, 2.0]),
            Point::new(Vec::new(), vec![6.0, 5.0])
        ];

        let expected = vec![
            Point::new(Vec::new(), vec![1.5, 0.5]),
            Point::new(Vec::new(), vec![2.0, 2.0]),
        ];

        let out = LocalOutlierFactor::new(2).get_k_nearest_neighbors(&points, &point).into_iter().map(|(_, p)| p).collect::<Vec<_>>();

        assert_eq!(out, expected);
    }
}

