mod decision_tree;
mod evolving;
mod k_nearest_neighbors;
mod kmeans;
mod least_squares;
mod local_outlier_factor;
mod logistic_regression;
mod multiple_linear_regression;
mod simple_linear_regression;

mod utils;

pub use decision_tree::DecisionTree;
pub use evolving::{
    Evolver,
    Learnable,
};
pub use k_nearest_neighbors::KNearestNeighbors;
pub use kmeans::KMeans;
pub use least_squares::LeastSquares;
pub use local_outlier_factor::LocalOutlierFactor;
pub use logistic_regression::LogisticRegression;
pub use multiple_linear_regression::MultipleLinearRegression;
pub use simple_linear_regression::SimpleLinearRegression;
pub use utils::{
    data::{
        Data,
        DataShape,
        DataValue,
    },
    error::Error,
};

pub mod tools {
    pub use crate::k_nearest_neighbors::HashableDataValue;
    pub use crate::utils::{
        calculations::*,
        data_preparations::NumerizeData,
    };
}
