use std::collections::HashMap;

use itertools::Itertools;

use super::data::Data;
use crate::{
    DataValue,
    Error,
};

pub struct NumerizeData(HashMap<String, HashMap<String, usize>>);

impl NumerizeData {
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    pub fn convert_data(&mut self, data: &Data) -> Data {
        let mut data = data.clone();

        for (n, mut column) in data.iter_mut_columns() {
            if !self
                .0
                .contains_key(n)
            {
                self.0
                    .insert(n.to_owned(), HashMap::new());
            }

            let map = self
                .0
                .get_mut(n)
                .expect("column not found in numerize map");

            for v in column.iter_mut() {
                *v = match v {
                    DataValue::Bool(b) => {
                        if *b {
                            DataValue::Integer(1)
                        } else {
                            DataValue::Integer(0)
                        }
                    },
                    DataValue::Null => DataValue::Integer(0),
                    DataValue::String(s) => {
                        if let Some(c) = map.get(&*s) {
                            DataValue::Integer(*c as i32)
                        } else {
                            let c = map.len() + 1;
                            map.insert(s.to_owned(), c);
                            DataValue::Integer(c as i32)
                        }
                    },
                    _ => continue,
                }
            }
        }

        data
    }
}

impl Default for NumerizeData {
    fn default() -> Self {
        Self::new()
    }
}

pub fn normalize_data(mut data: Data, columns: &[&str]) -> Result<Data, Error> {
    for (col_name, mut column) in data.iter_mut_columns() {
        if !columns.contains(&col_name) {
            continue;
        }

        let (min, max) = {
            let (min, max) = column
                .iter()
                .minmax()
                .into_option()
                .ok_or(Error::NoData)?;
            (min.as_float_value()?, max.as_float_value()?)
        };
        let div = max.sub(&min);

        for v in column.iter_mut() {
            *v = v.as_float_value()?
                .sub(&min)
                .div(&div);
        }
    }

    Ok(data)
}
