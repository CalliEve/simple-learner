use std::{
    cmp::Ordering,
    iter::{
        FromIterator,
        Sum,
    },
    path::Path,
};

use ndarray::{
    stack,
    Array1,
    Array2,
    ArrayView1,
    ArrayViewMut1,
    Axis,
};

use super::error::Error;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum DataShape {
    Float,
    Integer,
    String,
    Bool,
}

#[derive(Clone, Debug, PartialEq)]
pub enum DataValue {
    Float(f64),
    Integer(i32),
    String(String),
    Bool(bool),
    Null,
}

impl DataValue {
    pub fn as_float(&self) -> Option<f64> {
        match self {
            Self::Float(f) => Some(*f),
            Self::Integer(i) => Some(*i as f64),
            Self::Null => Some(0.0),
            _ => None,
        }
    }

    pub fn as_int(&self) -> Option<i32> {
        match self {
            Self::Integer(i) => Some(*i),
            Self::Float(f) => Some(f.round() as i32),
            Self::Null => Some(0),
            _ => None,
        }
    }

    pub fn as_bool(&self) -> bool {
        match self {
            Self::Bool(b) => *b,
            Self::Float(f) => *f != 0_f64,
            Self::Integer(i) => *i != 0_i32,
            Self::String(s) => !s.is_empty(),
            Self::Null => false,
        }
    }

    pub fn as_string(&self) -> String {
        match self {
            Self::Float(f) => f.to_string(),
            Self::Integer(i) => i.to_string(),
            Self::String(s) => s.to_owned(),
            Self::Bool(b) => b.to_string(),
            Self::Null => "".to_owned(),
        }
    }

    pub fn is_numeric(&self) -> bool {
        matches!(self, Self::Float(_) | Self::Integer(_) | Self::Null)
    }

    pub fn is_null(&self) -> bool {
        matches!(self, Self::Null)
    }

    pub fn is_bool(&self) -> bool {
        matches!(self, Self::Bool(_))
    }

    pub fn pow(&self, other: u32) -> Self {
        if !self.is_numeric() {
            panic!("can't raise a non-numeric value to a power")
        }

        match self {
            Self::Float(f) => f
                .powi(other as i32)
                .into(),
            Self::Integer(i) => i
                .pow(other)
                .into(),
            _ => unreachable!(),
        }
    }

    pub fn sqrt(&self) -> Self {
        self.as_float()
            .expect("can't get the square root of a non-numeric value")
            .sqrt()
            .into()
    }

    pub fn add(&self, other: &Self) -> Self {
        if !self.is_numeric() || !other.is_numeric() {
            panic!("Can't add non-numeric values together")
        }

        match self {
            Self::Float(f) => Self::Float(
                f + other
                    .as_float()
                    .expect("value is numeric but can't be made into a float, this is a bug"),
            ),
            Self::Integer(i) => Self::Integer(
                i + other
                    .as_int()
                    .expect("value is numeric but can't be made into an integer, this is a bug"),
            ),
            Self::Null => {
                if other.is_null() {
                    Self::Null
                } else {
                    other.add(self)
                }
            },
            _ => unreachable!(),
        }
    }

    pub fn sub(&self, other: &Self) -> Self {
        if !self.is_numeric() || !other.is_numeric() {
            panic!("Can't substract non-numeric values")
        }

        match self {
            Self::Float(f) => Self::Float(
                f - other
                    .as_float()
                    .expect("value is numeric but can't be made into a float, this is a bug"),
            ),
            Self::Integer(i) => Self::Integer(
                i - other
                    .as_int()
                    .expect("value is numeric but can't be made into an integer, this is a bug"),
            ),
            Self::Null => match other {
                Self::Float(f) => Self::Float(0.0 - f),
                Self::Integer(i) => Self::Integer(0 - i),
                Self::Null => Self::Null,
                _ => unreachable!(),
            },
            _ => unreachable!(),
        }
    }

    pub fn mul(&self, other: &Self) -> Self {
        if !self.is_numeric() || !other.is_numeric() {
            panic!("Can't multiply non-numeric values")
        }

        match self {
            Self::Float(f) => Self::Float(
                f * other
                    .as_float()
                    .expect("value is numeric but can't be made into a float, this is a bug"),
            ),
            Self::Integer(i) => Self::Integer(
                i * other
                    .as_int()
                    .expect("value is numeric but can't be made into an integer, this is a bug"),
            ),
            Self::Null => Self::Null,
            _ => unreachable!(),
        }
    }

    pub fn div(&self, other: &Self) -> Self {
        if !self.is_numeric() || !other.is_numeric() {
            panic!("Can't divide non-numeric values")
        }

        match self {
            Self::Float(f) => Self::Float(
                f / other
                    .as_float()
                    .expect("value is numeric but can't be made into a float, this is a bug"),
            ),
            Self::Integer(i) => Self::Integer(
                i / other
                    .as_int()
                    .expect("value is numeric but can't be made into an integer, this is a bug"),
            ),
            Self::Null => Self::Null,
            _ => unreachable!(),
        }
    }

    pub fn exp(&self) -> Self {
        if !self.is_numeric() {
            panic!("Can't get the exponent of non-numeric values")
        }

        match self {
            Self::Float(f) => Self::Float(f.exp()),
            Self::Integer(i) => Self::Float((*i as f64).exp()),
            Self::Null => Self::Float(1.0),
            _ => unreachable!(),
        }
    }

    pub fn ln(&self) -> Self {
        if !self.is_numeric() {
            panic!("Can't get the log of non-numeric values")
        }

        match self {
            Self::Float(f) => Self::Float(f.ln()),
            Self::Integer(i) => Self::Float((*i as f64).ln()),
            Self::Null => Self::Float(0_f64.ln()),
            _ => unreachable!(),
        }
    }

    pub fn max(&self, other: &Self) -> Result<Self, Error> {
        Ok(match self {
            Self::Float(f) if other.is_numeric() => f
                .max(
                    other
                        .as_float()
                        .expect("value is numeric but can't be made into a float, this is a bug"),
                )
                .into(),
            Self::Integer(i) if other.is_numeric() => i
                .max(
                    &other
                        .as_int()
                        .expect(
                            "value is numeric but can't be made into an integer, this is a bug",
                        ),
                )
                .to_owned()
                .into(),
            Self::Null => other.max(self)?,
            Self::String(s) if matches!(other, Self::String(_) | Self::Null) => s
                .max(&other.as_string())
                .to_owned()
                .into(),
            _ => return Err(Error::Incomparible),
        })
    }

    pub fn min(&self, other: &Self) -> Result<Self, Error> {
        Ok(match self {
            Self::Float(f) if other.is_numeric() => f
                .min(
                    other
                        .as_float()
                        .expect("value is numeric but can't be made into a float, this is a bug"),
                )
                .into(),
            Self::Integer(i) if other.is_numeric() => i
                .min(
                    &other
                        .as_int()
                        .expect(
                            "value is numeric but can't be made into an integer, this is a bug",
                        ),
                )
                .to_owned()
                .into(),
            Self::Null => other.min(self)?,
            Self::String(s) if matches!(other, Self::String(_) | Self::Null) => s
                .min(&other.as_string())
                .to_owned()
                .into(),
            _ => return Err(Error::Incomparible),
        })
    }

    pub fn as_float_value(&self) -> Result<Self, Error> {
        Ok(self.as_float().ok_or(Error::NotNumeric)?.into())
    }
}

impl std::fmt::Display for DataValue {
    fn fmt(&self, fmtr: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Float(f) => fmtr.write_str(&f.to_string()),
            Self::Integer(i) => fmtr.write_str(&i.to_string()),
            Self::String(s) => fmtr.write_str(s),
            Self::Bool(b) => fmtr.write_str(&b.to_string()),
            Self::Null => fmtr.write_str("null"),
        }
    }
}

impl Sum for DataValue {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(Self::Null, |acc, v| acc.add(&v))
    }
}

impl PartialOrd for DataValue {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            Self::Float(f) => f.partial_cmp(&other.as_float()?),
            Self::Integer(i) => i.partial_cmp(&other.as_int()?),
            Self::String(s) => s.partial_cmp(&other.as_string()),
            _ => None,
        }
    }
}

impl From<f64> for DataValue {
    fn from(f: f64) -> Self {
        Self::Float(f)
    }
}

impl From<i32> for DataValue {
    fn from(i: i32) -> Self {
        Self::Integer(i)
    }
}

impl From<String> for DataValue {
    fn from(s: String) -> Self {
        Self::String(s)
    }
}

impl From<&str> for DataValue {
    fn from(s: &str) -> Self {
        Self::String(s.to_owned())
    }
}

impl From<bool> for DataValue {
    fn from(b: bool) -> Self {
        Self::Bool(b)
    }
}

#[derive(Debug, Clone)]
pub struct Data {
    pub(crate) data: Array2<DataValue>,
    columns: Vec<String>,
    index: Array1<DataValue>,
}

impl Data {
    pub fn new(data: Array2<DataValue>) -> Self {
        Self {
            columns: vec!["".to_owned(); data.len_of(Axis(1))],
            index: Array1::from_vec(
                (0..data.len_of(Axis(0)))
                    .into_iter()
                    .map(|i| DataValue::Integer(i as i32))
                    .collect(),
            ),
            data,
        }
    }

    pub fn set_columns(&mut self, columns: Vec<String>) -> &mut Self {
        if columns.len()
            != self
                .columns
                .len()
        {
            panic!(
                "Incorrect columns length given, is {}, but should be {}",
                columns.len(),
                self.columns
                    .len()
            )
        }
        self.columns = columns;
        self
    }

    pub fn push_column<I, N>(&mut self, column: I, name: N) -> Result<&mut Self, Error>
    where
        I: IntoIterator<Item = DataValue>,
        N: ToString,
    {
        self.columns
            .push(name.to_string());
        self.data
            .push_column(
                column
                    .into_iter()
                    .collect::<Array1<DataValue>>()
                    .view(),
            )?;
        Ok(self)
    }

    pub fn min(&self) -> Vec<(&str, f64)> {
        self.columns
            .iter()
            .map(|s| s.as_str())
            .zip(
                self.data
                    .columns()
                    .into_iter()
                    .map(|c| {
                        c.into_iter()
                            .filter_map(DataValue::as_float)
                            .fold(f64::NAN, |x, y| x.min(y))
                    }),
            )
            .filter_map(|(c, v)| if v.is_nan() { None } else { Some((c, v)) })
            .collect()
    }

    pub fn max(&self) -> Vec<(&str, f64)> {
        self.columns
            .iter()
            .map(|s| s.as_str())
            .zip(
                self.data
                    .columns()
                    .into_iter()
                    .map(|c| {
                        c.into_iter()
                            .filter_map(DataValue::as_float)
                            .fold(f64::NAN, |x, y| x.max(y))
                    }),
            )
            .filter_map(|(c, v)| if v.is_nan() { None } else { Some((c, v)) })
            .collect()
    }

    pub fn avg(&self) -> Vec<(&str, f64)> {
        self.columns
            .iter()
            .map(|s| s.as_str())
            .zip(
                self.data
                    .columns()
                    .into_iter()
                    .map(|c| {
                        c.iter()
                            .map(|f| DataValue::as_float(f).unwrap_or(f64::NAN))
                            .sum::<f64>()
                            / c.len() as f64
                    }),
            )
            .filter_map(|(c, v)| if v.is_nan() { None } else { Some((c, v)) })
            .collect()
    }

    pub fn variance(&self) -> Vec<(&str, f64)> {
        let averages: Vec<f64> = self
            .data
            .columns()
            .into_iter()
            .map(|c| {
                c.iter()
                    .map(|f| DataValue::as_float(f).unwrap_or(f64::NAN))
                    .sum::<f64>()
                    / c.len() as f64
            })
            .collect();

        let variances = self
            .data
            .columns()
            .into_iter()
            .zip(averages)
            .map(|(col, avg)| {
                col.iter()
                    .map(|v| {
                        (v.as_float()
                            .unwrap_or(f64::NAN)
                            - avg)
                            .powi(2)
                    })
                    .collect::<Vec<f64>>()
            })
            .map(|c| {
                c.iter()
                    .sum::<f64>()
                    / c.len() as f64
            });

        self.columns
            .iter()
            .map(|s| s.as_str())
            .zip(variances)
            .filter_map(|(c, v)| if v.is_nan() { None } else { Some((c, v)) })
            .collect()
    }

    pub fn standard_deviation(&self) -> Vec<(&str, f64)> {
        self.variance()
            .into_iter()
            .map(|(c, v)| (c, v.sqrt()))
            .collect()
    }

    pub fn row_len(&self) -> usize {
        self.data
            .len_of(Axis(0))
    }

    pub fn column_len(&self) -> usize {
        self.columns
            .len()
    }

    pub fn len(&self) -> usize {
        self.data
            .len()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn iter_column_names(&self) -> impl Iterator<Item = &str> {
        self.columns
            .iter()
            .map(|s| s.as_str())
    }

    pub fn iter_columns(&self) -> impl Iterator<Item = (&str, ArrayView1<DataValue>)> {
        self.columns
            .iter()
            .map(|s| s.as_str())
            .zip(
                self.data
                    .columns()
                    .into_iter(),
            )
    }

    pub fn iter_mut_columns(&mut self) -> impl Iterator<Item = (&str, ArrayViewMut1<DataValue>)> {
        self.columns
            .iter()
            .map(|s| s.as_str())
            .zip(
                self.data
                    .columns_mut()
                    .into_iter(),
            )
    }

    pub fn iter_rows(&self) -> impl Iterator<Item = ArrayView1<DataValue>> {
        self.data
            .rows()
            .into_iter()
    }

    pub fn iter_mut_rows(&mut self) -> impl Iterator<Item = ArrayViewMut1<DataValue>> {
        self.data
            .rows_mut()
            .into_iter()
    }

    pub fn iter(&self)-> impl Iterator<Item = &DataValue> {
        self.data.iter()
    }
    
    pub fn iter_mut(&mut self)-> impl Iterator<Item = &mut DataValue> {
        self.data.iter_mut()
    }

    pub fn get_column<'a>(&self, column: &'a str) -> Option<(&'a str, ArrayView1<DataValue>)> {
        let index = self
            .columns
            .iter()
            .position(|c| c == column)?;
        Some((
            column,
            self.data
                .column(index),
        ))
    }

    pub fn get_row(&self, row: impl Into<DataValue>) -> Option<ArrayView1<DataValue>> {
        let index: DataValue = row.into();
        let row_index = self
            .index
            .iter()
            .position(|i| i == &index)?;
        Some(
            self.data
                .row(row_index),
        )
    }

    pub fn get(&self, column: &str, row: impl Into<DataValue>) -> Option<&DataValue> {
        let col_index = self
            .columns
            .iter()
            .position(|c| c == column)?;
        let index: DataValue = row.into();
        let row_index = self
            .index
            .iter()
            .position(|i| i == &index)?;
        Some(&self.data[[row_index, col_index]])
    }

    pub fn is_fully_numeric(&self) -> bool {
        for c in self
            .data
            .columns()
        {
            if !c
                .first()
                .unwrap_or(&DataValue::Null)
                .is_numeric()
            {
                return false;
            }
        }

        true
    }

    pub fn dot(&self, rhs: &Data) -> Result<Data, Error> {
        if self.column_len() != rhs.row_len() {
            return Err(Error::WrongDimensions);
        }

        if !self.is_fully_numeric() || !rhs.is_fully_numeric() {
            return Err(Error::NotNumeric);
        }

        let lhs_data = self
            .data
            .map(|v| {
                v.as_float()
                    .expect("data value is numeric but can't be a float")
            });
        let rhs_data = rhs
            .data
            .map(|v| {
                v.as_float()
                    .expect("data value is numeric but can't be a float")
            });

        let res = lhs_data
            .dot(&rhs_data)
            .map(|v| DataValue::Float(*v));

        Ok(Data {
            data: res,
            columns: rhs
                .columns
                .clone(),
            index: self
                .index
                .clone(),
        })
    }

    pub fn transpose(&self) -> Self {
        Self {
            data: self
                .data
                .t()
                .to_owned(),
            columns: self
                .index
                .map(|v| v.as_string())
                .to_vec(),
            index: self
                .columns
                .iter()
                .map(|v| DataValue::String(v.clone()))
                .collect(),
        }
    }

    pub fn make_index(&mut self, column: &str) -> Result<&mut Self, Error> {
        let index = self
            .columns
            .iter()
            .position(|c| c == column)
            .ok_or(Error::IndexOutOfRange)?;

        self.index = self
            .data
            .column(index)
            .to_owned();
        self.data = stack(
            Axis(1),
            &self
                .data
                .columns()
                .into_iter()
                .enumerate()
                .filter(|(i, _)| *i != index)
                .map(|(_, v)| v)
                .collect::<Vec<ArrayView1<DataValue>>>(),
        )
        .unwrap();
        self.columns
            .remove(index);
        Ok(self)
    }

    pub fn read_csv(path: impl AsRef<Path>, shape: &[DataShape]) -> Result<Self, Error> {
        let mut reader = csv::Reader::from_path(path)?;
        let mut table = Vec::new();
        for result in reader.records() {
            let record = result?;
            let mut row: Vec<DataValue> = Vec::new();

            for (entry, shape) in record
                .iter()
                .zip(shape.iter())
            {
                match shape {
                    DataShape::Float => row.push(
                        entry
                            .parse::<f64>()
                            .map_err(|_| Error::NotFitted)?
                            .into(),
                    ),
                    DataShape::Integer => row.push(
                        entry
                            .parse::<i32>()
                            .map_err(|_| Error::NotFitted)?
                            .into(),
                    ),
                    DataShape::String => row.push(
                        entry
                            .to_owned()
                            .into(),
                    ),
                    DataShape::Bool => row.push(
                        entry
                            .parse::<bool>()
                            .map_err(|_| Error::NotFitted)?
                            .into(),
                    ),
                }
            }

            table.push(row)
        }

        Ok(table
            .into_iter()
            .collect())
    }
}

/*impl<T> FromIterator<(T, T)> for Data where T: Into<DataValue> {
    fn from_iter<I: IntoIterator<Item=(T, T)>>(iter: I) -> Self {
        let v: Vec<DataValue> = iter.into_iter().map(|(x, y)| vec![x.into(), y.into()]).flatten().collect();
        Self::new(Array2::from_shape_vec((v.len() / 2, 2), v).unwrap())
    }
}*/

impl<A, T> FromIterator<A> for Data
where
    A: IntoIterator<Item = T>,
    T: Into<DataValue>,
{
    fn from_iter<I: IntoIterator<Item = A>>(iter: I) -> Self {
        let v_of_vs: Vec<Vec<DataValue>> = iter
            .into_iter()
            .map(|inner| {
                inner
                    .into_iter()
                    .map(|d| d.into())
                    .collect::<Vec<DataValue>>()
            })
            .collect();

        let column_count = if let Some(r) = v_of_vs.first() {
            r.len()
        } else {
            panic!("A data object can't be made from an empty iterator")
        };

        if v_of_vs
            .iter()
            .any(|r| r.len() != column_count)
        {
            panic!("For a data object to be made, all columns must be of equal length")
        }

        let v: Vec<DataValue> = v_of_vs
            .into_iter()
            .flatten()
            .collect();
        Self::new(Array2::from_shape_vec((v.len() / column_count, column_count), v).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use ndarray::array;

    use super::*;

    #[test]
    fn data_avg() {
        let data = Data::new(array![
            [6.into(), 5.0.into(), "test".into()],
            [7.into(), 3.0.into(), "test".into()],
            [8.into(), 10.0.into(), "test".into()],
        ]);

        assert_eq!(vec![("", 7.0), ("", 6.0)], data.avg())
    }

    #[test]
    fn data_max() {
        let data = Data::new(array![
            [6.into(), 5.0.into(), "test".into()],
            [7.into(), 3.0.into(), "test".into()],
            [8.into(), 10.0.into(), "test".into()],
        ]);

        assert_eq!(vec![("", 8.0), ("", 10.0)], data.max())
    }

    #[test]
    fn data_min() {
        let data = Data::new(array![
            [6.into(), 5.0.into(), "test".into()],
            [7.into(), 3.0.into(), "test".into()],
            [8.into(), 10.0.into(), "test".into()],
        ]);

        assert_eq!(vec![("", 6.0), ("", 3.0)], data.min())
    }

    #[test]
    fn data_len_methods() {
        let data = Data::new(array![
            [6.into(), 5.0.into()],
            [7.into(), 3.0.into()],
            [8.into(), 10.0.into()],
        ]);

        assert_eq!(3, data.row_len());
        assert_eq!(2, data.column_len());
        assert_eq!(6, data.len());
    }

    #[test]
    fn data_get_methods() {
        let mut data = Data::new(array![
            [6.into(), 5.0.into()],
            [7.into(), 3.0.into()],
            [8.into(), 10.0.into()],
        ]);
        data.set_columns(vec!["col_1".to_owned(), "col_2".to_owned()]);

        assert_eq!(Some(array![7.into(), 3.0.into()].view()), data.get_row(1));
        assert_eq!(
            Some(("col_2", array![5.0.into(), 3.0.into(), 10.0.into()].view())),
            data.get_column("col_2")
        );
        assert_eq!(Some(&7.into()), data.get("col_1", 1))
    }

    #[test]
    fn data_standard_deviation() {
        let data: Data = Data::new(array![
            [2.into()],
            [4.into()],
            [4.into()],
            [4.into()],
            [5.into()],
            [5.into()],
            [7.into()],
            [9.into()]
        ]);

        assert_eq!(vec![("", 2.0)], data.standard_deviation());
    }
}
