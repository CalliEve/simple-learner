use ndarray::Array2;

pub fn rref(mut matrix: Array2<f64>) -> Array2<f64> {
    for row_n in 0..matrix.nrows() {
        let mut row = matrix.row_mut(row_n);
        let non_zero = row
            .iter()
            .enumerate()
            .find(|(_, v)| **v != 0f64)
            .map(|(i, v)| (i, *v));

        if non_zero.is_none() {
            continue;
        }

        let non_zero = non_zero.unwrap();
        row.iter_mut()
            .for_each(|v| *v /= non_zero.1);
        let row = row.to_vec();

        for (i_r, mut inner_row_n) in matrix
            .rows_mut()
            .into_iter()
            .enumerate()
        {
            if i_r == row_n {
                continue;
            }

            let mult = inner_row_n
                .get(non_zero.0)
                .copied()
                .unwrap();

            if mult == 0f64 {
                continue;
            }

            for (i_c, x) in inner_row_n
                .iter_mut()
                .enumerate()
            {
                *x -= mult * row[i_c]
            }
        }
    }

    matrix
}

#[inline]
pub fn euclidian_distance(a: &[f64], b: &[f64]) -> f64 {
    assert_eq!(a.len(), b.len());

    a.iter()
        .zip(b.iter())
        .map(|(x, y)| (x - y).powi(2))
        .sum::<f64>()
        .sqrt()
}

#[cfg(test)]
mod tests {
    use ndarray::array;

    use super::*;

    #[test]
    fn test_rref() {
        let input = array![[2, 8, 4, 2], [2, 5, 1, 5], [4, 10, -1, 1]].map(|i| *i as f64);
        let check = array![[1, 0, 0, 11], [0, 1, 0, -4], [0, 0, 1, 3]].map(|i| *i as f64);

        let res = rref(input);

        assert_eq!(res, check);
    }
}
