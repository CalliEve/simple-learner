pub enum Error {
    CSV(csv::Error),
    NDArray(ndarray::ShapeError),
    NoData,
    WrongDimensions,
    NotFitted,
    IndexOutOfRange,
    Incomparible,
    NotNumeric,
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::CSV(err) => err.fmt(f),
            Self::NDArray(err) => err.fmt(f),
            Self::NoData => f.write_str("No Data was provided"),
            Self::WrongDimensions => f.write_str("The data was in an invalid amount of dimensions or not all data points had the same dimensions"),
            Self::NotFitted => f.write_str("The data has not been fitted to the algorithm yet"),
            Self::IndexOutOfRange => f.write_str("The given index is higher than size of the vec/array or does not exist"),
            Self::Incomparible => f.write_str("The given data types can't be compared with each other"),
            Self::NotNumeric => f.write_str("Non-numeric data was provided to an operation that only supports numeric data")
        }
    }
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::CSV(err) => f.write_str(&format!("CSV Error: {:?}", err)),
            Self::NDArray(err) => f.write_str(&format!("ndarray Error: {:?}", err)),
            Self::NoData => f.write_str("No Data"),
            Self::WrongDimensions => f.write_str("Wrong Dimensions"),
            Self::NotFitted => f.write_str("Not Fitted"),
            Self::IndexOutOfRange => f.write_str("Index Out Of Range"),
            Self::Incomparible => f.write_str("Incomparible"),
            Self::NotNumeric => f.write_str("Not Numeric"),
        }
    }
}

impl From<csv::Error> for Error {
    fn from(err: csv::Error) -> Self {
        Error::CSV(err)
    }
}

impl From<ndarray::ShapeError> for Error {
    fn from(err: ndarray::ShapeError) -> Self {
        Error::NDArray(err)
    }
}
