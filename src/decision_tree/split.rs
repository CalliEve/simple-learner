use rand::Rng;

use super::{
    Node,
    NodeContent,
    Operation,
};
use crate::DataValue;

/// Split the Data on the column and method with the most information gain,
/// returning the created [`NodeContent`]
pub(super) fn split(data: &[(&str, Vec<DataValue>)], check_col: &[DataValue]) -> NodeContent {
    let full_gini = gini_impurity(
        &check_col
            .iter()
            .collect::<Vec<&DataValue>>(),
    );

    data.iter()
        .enumerate()
        .map(|(i, (n, col))| {
            match col
                .iter()
                .find(|i| !matches!(i, DataValue::Null))
                .cloned()
            {
                Some(DataValue::Float(_)) | Some(DataValue::Integer(_)) => {
                    split_number((n, i, col), check_col, full_gini)
                },
                Some(DataValue::Bool(_)) => split_bool((n, i, col), check_col, full_gini),
                Some(DataValue::String(_)) => split_string((n, i, col), check_col, full_gini),
                _ => (f64::NEG_INFINITY, NodeContent::default()),
            }
        })
        .fold(Option::<(f64, NodeContent)>::None, |acc, val| {
            if let Some(best) = &acc {
                if best
                    .0
                    .is_nan()
                    || best.0 < val.0
                {
                    return Some(val);
                }
            } else {
                return Some(val);
            };

            acc
        })
        .unwrap()
        .1
}

/// Split a boolean column, returning the information gain and a
/// [`NodeContent`].
fn split_bool(
    col: (&str, usize, &[DataValue]),
    check_col: &[DataValue],
    full_gini: f64,
) -> (f64, NodeContent) {
    let true_indexes = col
        .2
        .iter()
        .enumerate()
        .fold(Vec::new(), |mut acc, (i, val)| {
            if val.as_bool() {
                acc.push(i);
            }
            acc
        });

    let (left, right) = split_check_col(&true_indexes, check_col);

    (
        full_gini - ((gini_impurity(&left) + gini_impurity(&right)) / 2.0),
        NodeContent {
            right: leaf_or_unknown(&right),
            left: leaf_or_unknown(&left),
            operation: Operation::IsTrue,
            value: DataValue::Null,
            column: col.1,
            column_name: col
                .0
                .to_owned(),
        },
    )
}

/// Split a string column, returning the information gain and a [`NodeContent`].
/// This calculates the information gain of all the different strings and
/// chooses the one with the most gain.
fn split_string(
    col: (&str, usize, &[DataValue]),
    check_col: &[DataValue],
    full_gini: f64,
) -> (f64, NodeContent) {
    col.2
        .iter()
        .fold(Vec::new(), |mut acc, val| {
            if !acc.contains(val) {
                acc.push(val.clone());
            }
            acc
        })
        .into_iter()
        .map(|item| split_specific_string(item, col, check_col, full_gini))
        .fold(Option::<(f64, NodeContent)>::None, |acc, val| {
            if let Some(best) = acc {
                if best
                    .0
                    .is_nan()
                    || best.0 < val.0
                {
                    return Some(val);
                }
            };

            Some(val)
        })
        .unwrap()
}

/// Split a string column on the value being equal to the given `to_equal` param
/// or not, returning the information gain and a [`NodeContent`].
fn split_specific_string(
    to_equal: DataValue,
    col: (&str, usize, &[DataValue]),
    check_col: &[DataValue],
    full_gini: f64,
) -> (f64, NodeContent) {
    let true_indexes = col
        .2
        .iter()
        .enumerate()
        .fold(Vec::new(), |mut acc, (i, val)| {
            if val == &to_equal {
                acc.push(i);
            }
            acc
        });

    let (left, right) = split_check_col(&true_indexes, check_col);

    (
        full_gini - ((gini_impurity(&left) + gini_impurity(&right)) / 2.0),
        NodeContent {
            right: leaf_or_unknown(&right),
            left: leaf_or_unknown(&left),
            operation: Operation::IsEqual,
            value: to_equal,
            column: col.1,
            column_name: col
                .0
                .to_owned(),
        },
    )
}

/// Split a number column, returning the information gain and a [`NodeContent`].
/// For now uses the column average as the threshold for this.
fn split_number(
    col: (&str, usize, &[DataValue]),
    check_col: &[DataValue],
    full_gini: f64,
) -> (f64, NodeContent) {
    let mut rng = rand::thread_rng();
    let (max, min) = col
        .2
        .iter()
        .filter_map(DataValue::as_float)
        .fold((f64::NAN, f64::NAN), |(max, min), y| {
            (max.max(y), min.min(y))
        });

    if (min - max).abs() < f64::EPSILON {
        return (f64::NEG_INFINITY, NodeContent::default());
    }

    std::iter::repeat_with(|| rng.gen_range(min..max))
        .take(10)
        .map(|item| split_specific_number(item.into(), col, check_col, full_gini))
        .fold(Option::<(f64, NodeContent)>::None, |acc, val| {
            if let Some(best) = acc {
                if best
                    .0
                    .is_nan()
                    || best.0 < val.0
                {
                    return Some(val);
                }
            };

            Some(val)
        })
        .unwrap()
}

/// Split a number column on the value being bigger than the given `to_equal`
/// param or not, returning the information gain and a [`NodeContent`].
fn split_specific_number(
    to_equal: DataValue,
    col: (&str, usize, &[DataValue]),
    check_col: &[DataValue],
    full_gini: f64,
) -> (f64, NodeContent) {
    let true_indexes = col
        .2
        .iter()
        .enumerate()
        .fold(Vec::new(), |mut acc, (i, val)| {
            if val > &to_equal {
                acc.push(i);
            }
            acc
        });

    let (left, right) = split_check_col(&true_indexes, check_col);

    (
        full_gini - ((gini_impurity(&left) + gini_impurity(&right)) / 2.0),
        NodeContent {
            right: leaf_or_unknown(&right),
            left: leaf_or_unknown(&left),
            operation: Operation::BiggerThan,
            value: to_equal,
            column: col.1,
            column_name: col
                .0
                .to_owned(),
        },
    )
}

/// Splits a column into 2 based on if the index is in the given slice of
/// indexes or not.
pub(super) fn split_check_col<'a>(
    indexes: &[usize],
    check_col: &'a [DataValue],
) -> (Vec<&'a DataValue>, Vec<&'a DataValue>) {
    check_col
        .iter()
        .enumerate()
        .fold(
            (Vec::new(), Vec::new()),
            |(mut left, mut right), (i, val)| {
                if indexes.contains(&i) {
                    right.push(val)
                } else {
                    left.push(val)
                }

                (left, right)
            },
        )
}

/// Returns if the Node is a leaf node or if it is unknown; based on if the
/// values given are all true or all false, making it a leaf node, or else
/// making it unknown.
fn leaf_or_unknown(values: &[&DataValue]) -> Node {
    if values.is_empty() {
        return Node::Leaf(false);
    }

    if values
        .iter()
        .all(|v| v.as_bool())
        || values
            .iter()
            .all(|v| !v.as_bool())
    {
        Node::Leaf(values[0].as_bool())
    } else {
        Node::Unknown
    }
}

/// Calculates the gini impurity of the column of bools.
fn gini_impurity(col: &[&DataValue]) -> f64 {
    let truths = col
        .iter()
        .fold(0.0, |acc, val| if val.as_bool() { acc + 1.0 } else { acc });
    1.0 - (truths / col.len() as f64) - ((col.len() as f64 - truths) / col.len() as f64)
}
