mod display;
mod split;

use std::fmt::{
    self,
    Display,
};

use display::display_tree;

use crate::{
    Data,
    DataValue,
    Error,
};

/// The Decision Tree itself. This struct contains the public face for all ways
/// to interact with the decision tree algorithm.
#[derive(Debug, Clone)]
pub struct DecisionTree {
    root: Option<Node>,
}

/// A node in the decision tree. This node can be a Leaf or a Node.
/// If a Node, it has children. The Unknown type is only used as a placeholder
/// and none should remain when finished with training.
#[derive(Debug, Clone)]
#[allow(clippy::enum_variant_names)]
enum Node {
    Leaf(bool),
    Node(Box<NodeContent>),
    Unknown,
}

/// The contents of a [`Node::Node`], this holds the data needed to make
/// decisions and the children of this node.
#[derive(Debug, Clone)]
struct NodeContent {
    right: Node,
    left: Node,
    operation: Operation,
    value: DataValue,
    column: usize,
    column_name: String,
}

/// The operation used by the decision in a node.
#[derive(Debug, Clone, Copy)]
enum Operation {
    IsTrue,
    BiggerThan,
    IsEqual,
}

impl DecisionTree {
    /// Create a new and empty DecisionTree.
    pub fn new() -> Self {
        Self {
            root: None,
        }
    }

    /// Train a DecisionTree. Calling this method is required before doing any
    /// other operation with it.
    pub fn train(&mut self, data: Data, truth_column: &str) -> Result<(), Error> {
        // convert the Data object to a Vec of columns
        let mut columns = data
            .iter_columns()
            .map(|(n, col)| (n, col.to_vec()))
            .collect::<Vec<(&str, Vec<DataValue>)>>();

        // get the column that has the
        let column_index = columns
            .iter()
            .position(|(n, _)| *n == truth_column)
            .ok_or(Error::NoData)?;
        let truth_column = columns
            .remove(column_index)
            .1;
        if truth_column
            .get(0)
            .map_or(true, |v| !v.is_bool())
        {
            return Err(Error::NoData);
        }

        self.root = Some(Node::Node(NodeContent::train(&columns, &truth_column)));

        Ok(())
    }

    /// Predicts if the result is true or false based on the trained
    /// DecisionTree. Returns [`Error::NotFitted`] if [`Self::train`] hasn't
    /// been called yet.
    pub fn predict(&self, input: &[DataValue]) -> Result<bool, Error> {
        self.root
            .iter()
            .map(|n| n.calculate(input))
            .next()
            .ok_or(Error::NotFitted)
    }

    /// Returns the depth of the DecisionTree, return 0 if empty.
    pub fn depth(&self) -> usize {
        match &self.root {
            Some(r) => r.depth(),
            None => 0,
        }
    }
}

impl Default for DecisionTree {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for DecisionTree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let node = if let Some(node) = self
            .root
            .clone()
        {
            node
        } else {
            return write!(f, "Empty Tree");
        };

        f.write_str(&display_tree(node))
    }
}

impl Node {
    /// A recursive method to get to the correct leaf node according to the
    /// given data.
    fn calculate(&self, input: &[DataValue]) -> bool {
        match self {
            Self::Leaf(b) => *b,
            Self::Node(node) => {
                if node
                    .operation
                    .check(&input[node.column], &node.value)
                {
                    node.right
                        .calculate(input)
                } else {
                    node.left
                        .calculate(input)
                }
            },
            Self::Unknown => panic!("Reached unknown node, this should never happen"),
        }
    }

    /// A recursive method to calculate the amount of nodes under the current
    /// node.
    fn depth(&self) -> usize {
        match self {
            Self::Leaf(_) => 0,
            Self::Node(n) => {
                1 + if n
                    .left
                    .depth()
                    > n.right
                        .depth()
                {
                    n.left
                        .depth()
                } else {
                    n.right
                        .depth()
                }
            },
            Self::Unknown => panic!("Reached unknown node, this should never happen"),
        }
    }
}

impl Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&match self {
            Self::Leaf(v) => {
                format!("leaf: {}", v)
            },
            Self::Node(n) => {
                format!("{}", n)
            },
            Self::Unknown => panic!("Reached unknown node, this should never happen"),
        })
    }
}

impl NodeContent {
    /// Creates a new `NodeContent` based on the given data columns and the
    /// column with output data. This method is recursive and will call this
    /// method of its child nodes till all branches end in leaf nodes.
    fn train(data: &[(&str, Vec<DataValue>)], check_col: &[DataValue]) -> Box<Self> {
        let mut content = split::split(data, check_col);

        let ((left, left_check), (right, right_check)) = content.split_data(data, check_col);
        if matches!(content.right, Node::Unknown) {
            content.right = Node::Node(NodeContent::train(&right, &right_check));
        }
        if matches!(content.left, Node::Unknown) {
            content.left = Node::Node(NodeContent::train(&left, &left_check));
        }

        Box::new(content)
    }

    /// Splits the given data columns and truth column into two based on the
    /// condition in the current node, returning a left and right dataset.
    #[allow(clippy::type_complexity)]
    fn split_data<'a>(
        &self,
        data: &[(&'a str, Vec<DataValue>)],
        check_col: &[DataValue],
    ) -> (
        (Vec<(&'a str, Vec<DataValue>)>, Vec<DataValue>),
        (Vec<(&'a str, Vec<DataValue>)>, Vec<DataValue>),
    ) {
        // Get all the row indexes where the condition is true.
        let indexes = data[self.column]
            .1
            .iter()
            .enumerate()
            .filter(|(_, v)| {
                self.operation
                    .check(v, &self.value)
            })
            .map(|(i, _)| i)
            .collect::<Vec<usize>>();

        // Split the data columns. This gives two new data sets, both a right and a left
        // with the rows that matched the current node's condition being added
        // to the right and the other rows to the left dataset.
        let (left, right) = data
            .iter()
            .map(|(n, col)| {
                (
                    (
                        *n,
                        col.iter()
                            .enumerate()
                            .filter(|(i, _)| !indexes.contains(i))
                            .map(|(_, v)| v)
                            .cloned()
                            .collect(),
                    ),
                    (
                        *n,
                        col.iter()
                            .enumerate()
                            .filter(|(i, _)| indexes.contains(i))
                            .map(|(_, v)| v)
                            .cloned()
                            .collect(),
                    ),
                )
            })
            .fold(
                (Vec::new(), Vec::new()),
                |(mut left, mut right), (l_item, r_item)| {
                    left.push(l_item);
                    right.push(r_item);
                    (left, right)
                },
            );

        let (left_check, right_check) = split::split_check_col(&indexes, check_col);

        (
            (
                left,
                left_check
                    .into_iter()
                    .cloned()
                    .collect(),
            ),
            (
                right,
                right_check
                    .into_iter()
                    .cloned()
                    .collect(),
            ),
        )
    }
}

impl Operation {
    /// Check if the operation returns true for the given data values.
    fn check(&self, value: &DataValue, other: &DataValue) -> bool {
        match self {
            Self::IsTrue => value.as_bool(),
            Self::IsEqual => value == other,
            Self::BiggerThan => value > other,
        }
    }
}

impl Display for NodeContent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let sign = match self.operation {
            Operation::IsTrue => return write!(f, "is true"),
            Operation::BiggerThan => ">",
            Operation::IsEqual => "==",
        };

        write!(f, "{} {}", sign, self.value)
    }
}

impl Default for NodeContent {
    fn default() -> Self {
        Self {
            left: Node::Unknown,
            right: Node::Unknown,
            operation: Operation::IsTrue,
            value: DataValue::Null,
            column: 0,
            column_name: String::new(),
        }
    }
}

impl Display for Operation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match self {
            Self::IsTrue => "is true",
            Self::IsEqual => "equals",
            Self::BiggerThan => "is bigger than",
        })
    }
}
