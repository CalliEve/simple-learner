use std::fmt::Write;

use super::Node;

/// Represents which side we are on compared to the parent node.
#[derive(Debug, PartialEq, Clone, Copy)]
enum Side {
    Left,
    Right,
    Up,
}

/// An element of the tree structure. It represents the path between the tree
/// nodes.
#[derive(Debug, PartialEq, Clone, Copy)]
enum DisplayElement {
    TrunkSpace,
    SpaceLeft,
    SpaceRight,
    SpaceSpace,
    Root,
}

impl DisplayElement {
    fn string(&self) -> String {
        match *self {
            DisplayElement::TrunkSpace => "    │   ".to_string(),
            DisplayElement::SpaceRight => "    ┌───".to_string(),
            DisplayElement::SpaceLeft => "    └───".to_string(),
            DisplayElement::SpaceSpace => "        ".to_string(),
            DisplayElement::Root => "├──".to_string(),
        }
    }
}

/// Returns a string representation of the binary NERDTreeToggle, of which the
/// given node is the root.
pub(super) fn display_tree(root: Node) -> String {
    let mut res = String::new();
    display(true, root, Side::Up, &Vec::new(), &mut res);
    res
}

/// Display a node and all that is under it by writing it to `f`.
/// This function is recursive and will call itself till all the nodes under the
/// given node have been writen to `f` too.
fn display(is_root: bool, node: Node, side: Side, e: &[DisplayElement], f: &mut dyn Write) {
    // split off if the node is a leaf, those have to be handled separately.
    let node = match node {
        Node::Node(n) => n,
        Node::Leaf(l) => return display_leaf(is_root, l, side, e, f),
        Node::Unknown => return,
    };

    let mut elems = e.to_owned();
    let mut tail = DisplayElement::SpaceSpace;

    if side == Side::Left {
        elems.push(DisplayElement::TrunkSpace);
    } else {
        elems.push(DisplayElement::SpaceSpace);
    }

    // First handle the right-hand side of the tree
    display(false, node.right, Side::Right, &elems, f);

    let hindex = elems.len() - 1;
    if is_root {
        elems[hindex] = DisplayElement::Root;
        tail = DisplayElement::SpaceSpace;
    } else if side == Side::Right {
        elems[hindex] = DisplayElement::SpaceRight;
        tail = DisplayElement::TrunkSpace;
    } else if side == Side::Left {
        elems[hindex] = DisplayElement::SpaceLeft;
    }

    // Write all elems to f and finish up with the node contents itself.
    for e in elems.clone() {
        let _ = write!(f, "{}", e.string());
    }
    let _ = write!(f, "{key:>width$} ", key = &node.column_name, width = 2);
    let _ = writeln!(
        f,
        "{value:>width$}",
        value = format!(
            "{} {}",
            node.operation,
            if matches!(node.value, crate::DataValue::Null) {
                "".into()
            } else {
                node.value
            }
        ),
        width = 4
    );

    // finish up by continuing with the left-hand side of the tree
    elems[hindex] = tail;
    display(false, node.left, Side::Left, &elems, f);
}

/// Writes a tree leaf to `f`.
fn display_leaf(
    is_root: bool,
    leaf_content: bool,
    side: Side,
    e: &[DisplayElement],
    f: &mut dyn Write,
) {
    let mut elems = e.to_owned();

    elems.push(DisplayElement::SpaceSpace);

    let hindex = elems.len() - 1;

    if is_root {
        elems[hindex] = DisplayElement::Root;
    } else if side == Side::Right {
        elems[hindex] = DisplayElement::SpaceRight;
    } else if side == Side::Left {
        elems[hindex] = DisplayElement::SpaceLeft;
    }

    // Write the display elements to f and then lastly add the leaf
    for e in elems.clone() {
        let _ = write!(f, "{}", e.string());
    }
    let _ = writeln!(
        f,
        " {key:>width$}",
        key = leaf_content.to_string(),
        width = 2
    );
}
