use rand::{
    prelude::SliceRandom,
    Rng,
};

use crate::{
    Data,
    DataValue,
    Error,
};

#[derive(Debug, Clone)]
pub struct LogisticRegression {
    weights: Vec<DataValue>,
    d: DataValue,
    data: Data,
    dep_column_index: usize,
}

impl LogisticRegression {
    pub fn new(data: Data, dep_column: impl ToString) -> Result<LogisticRegression, Error> {
        let mut rng = rand::thread_rng();
        let dep_column = dep_column.to_string();

        let dep_column_index = data
            .iter_column_names()
            .position(|c| c == dep_column)
            .ok_or(Error::NoData)?;

        data.iter_rows()
            .next()
            .map_or(Err(Error::NoData), |r| {
                if r.iter()
                    .any(|f| !f.is_numeric())
                {
                    Err(Error::NotNumeric)
                } else {
                    Ok(())
                }
            })?;

        Ok(Self {
            weights: (0..(data.column_len() - 1))
                .into_iter()
                .map(|_| {
                    rng.gen_range(-2.0..2.0)
                        .into()
                })
                .collect(),
            d: rng
                .gen::<f64>()
                .into(),
            data,
            dep_column_index,
        })
    }

    pub fn fit(&mut self, mut iterations: usize) {
        let mut rng = rand::thread_rng();

        while iterations > 0 {
            iterations -= 1;

            for row in self
                .data
                .iter_rows()
            {
                let mut row = row.to_vec();
                let y = row.remove(self.dep_column_index);

                row.shuffle(&mut rng);

                let y_predicted = self.predict(&row);

                self.weights = row
                    .iter()
                    .zip(&self.weights)
                    .map(|(x, w)| update(w, x, &y, &y_predicted))
                    .collect();
                self.d = update(&self.d, &1_i32.into(), &y, &y_predicted);
            }
        }
    }

    fn predict(&self, data: &[DataValue]) -> DataValue {
        activation(
            &data
                .iter()
                .zip(
                    self.weights
                        .iter(),
                )
                .map(|(x, w)| x.mul(w))
                .sum::<DataValue>()
                .add(&self.d),
        )
    }

    pub fn guess(&self, data: &[DataValue]) -> Result<DataValue, Error> {
        if data.len()
            != self
                .weights
                .len()
        {
            return Err(Error::WrongDimensions);
        }

        let predicted = self.predict(data);

        if predicted > 0.5.into() {
            Ok(1_i32.into())
        } else {
            Ok(0_i32.into())
        }
    }

    pub fn get_loss(&self, data: &[DataValue], correct: &DataValue) -> Result<DataValue, Error> {
        let y_predicted = self.guess(data)?;

        Ok(loss(correct, &y_predicted))
    }
}

fn update(weight: &DataValue, x: &DataValue, y: &DataValue, y_predicted: &DataValue) -> DataValue {
    weight.sub(
        &y_predicted
            .sub(y)
            .mul(x)
            .mul(&0.1.into()),
    )
}

fn activation(v: &DataValue) -> DataValue {
    DataValue::from(1.0).div(
        &DataValue::from(1.0).add(
            &v.mul(&(-1_f64).into())
                .exp(),
        ),
    )
}

fn loss(y: &DataValue, y_predicted: &DataValue) -> DataValue {
    DataValue::from(-1.0)
        .mul(&y_predicted.ln())
        .sub(
            &DataValue::from(1.0)
                .sub(y)
                .mul(
                    &DataValue::from(1.0)
                        .sub(y_predicted)
                        .ln(),
                ),
        )
}
