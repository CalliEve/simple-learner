use std::iter::repeat;

use learner::{
    tools::HashableDataValue,
    Data,
    DataValue,
    KNearestNeighbors,
};
use plotters::prelude::*;

fn main() {
    let mut data: Data = vec![
        vec![113, 3],
        vec![98, 31],
        vec![65, 20],
        vec![84, 36],
        vec![23, 23],
        vec![119, 15],
        vec![55, 27],
        vec![132, 24],
        vec![180, 8],
        vec![188, 7],
        vec![265, 26],
        vec![208, 32],
        vec![259, 14],
        vec![208, 22],
        vec![222, 22],
        vec![230, 21],
        vec![192, 20],
        vec![258, 16],
        vec![212, 32],
        vec![197, 26],
        vec![196, 31],
        vec![233, 18],
        vec![184, 35],
        vec![320, 30],
        vec![389, 11],
        vec![439, 13],
        vec![461, 25],
        vec![466, 20],
        vec![401, 21],
        vec![412, 18],
    ]
    .into_iter()
    .collect();

    data.set_columns(vec!["X".to_owned(), "Y".to_owned()]);
    data.push_column(
        repeat(1i32)
            .take(8)
            .chain(repeat(2).take(15))
            .chain(repeat(3).take(7))
            .map(DataValue::Integer)
            .by_ref(),
        "Z",
    )
    .unwrap();

    let model = KNearestNeighbors::from_data(5, data.clone(), "Z").unwrap();

    draw(data, model);
}

fn draw(data: Data, mut model: KNearestNeighbors<HashableDataValue>) {
    let new_points: Vec<(f32, f32, i32)> = vec![
        vec![110.0, 30.0],
        vec![40.0, 23.0],
        vec![80.0, 11.0],
        vec![100.0, 13.0],
        vec![120.0, 22.0],
        vec![111.0, 21.3],
        vec![210.0, 21.5],
        vec![233.0, 33.0],
        vec![210.0, 9.0],
        vec![290.4, 15.2],
        vec![276.0, 12.4],
        vec![173.3, 31.3],
        vec![223.1, 26.1],
        vec![267.7, 15.6],
        vec![420.0, 18.0],
        vec![350.0, 25.0],
        vec![380.1, 16.2],
        vec![98.9, 7.2],
    ]
    .into_iter()
    .map(|p| {
        (
            p[0] as f32,
            p[1] as f32,
            model
                .guess_and_add_floats(&p)
                .unwrap()
                .as_int()
                .unwrap(),
        )
    })
    .collect();

    let root = BitMapBackend::new("graph.png", (960, 720)).into_drawing_area();

    root.fill(&WHITE)
        .unwrap();
    let root = root.margin(10, 10, 10, 10);

    let mut chart = ChartBuilder::on(&root)
        .caption(
            "K Nearest Neighbors Clustering",
            ("sans-serif", 30).into_font(),
        )
        .x_label_area_size(20)
        .y_label_area_size(40)
        .build_cartesian_2d(0f32..500f32, 0f32..40f32)
        .unwrap();

    chart
        .configure_mesh()
        .x_labels(5)
        .y_labels(5)
        .draw()
        .unwrap();

    for kind in 1..=3 {
        let color = match kind {
            1 => RED,
            2 => BLUE,
            3 => GREEN,
            _ => unreachable!(),
        };

        chart
            .draw_series(PointSeries::of_element(
                new_points
                    .iter()
                    .filter(|(_, _, k)| *k == kind)
                    .map(|(x, y, _)| (*x, *y)),
                3,
                &color,
                &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
            ))
            .unwrap()
            .label(format!("Cluster {}", kind))
            .legend(move |(x, y)| Circle::new((x + 11, y), 3, color.filled()));

        chart
            .draw_series(PointSeries::of_element(
                data.iter_rows()
                    .map(|r| {
                        (
                            r[0].as_float()
                                .unwrap() as f32,
                            r[1].as_float()
                                .unwrap() as f32,
                            r[2].as_int()
                                .unwrap(),
                        )
                    })
                    .filter(|(_, _, k)| *k == kind)
                    .map(|(x, y, _)| (x, y)),
                3,
                &color,
                &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.stroke_width(1)),
            ))
            .unwrap()
            .label(format!("Initial data {}", kind))
            .legend(move |(x, y)| Circle::new((x + 11, y), 3, color.stroke_width(1)));
    }

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .label_font(("sans-serif", 15).into_font())
        .position(SeriesLabelPosition::UpperRight)
        .draw()
        .unwrap();

    root.present()
        .unwrap();
}
