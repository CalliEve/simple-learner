use std::fmt::{
    self,
    Debug,
};
use std::{
    f64::consts::PI,
    thread,
};

use crossbeam::channel::{
    bounded,
    Receiver as ChannelReceiver,
    Sender as ChannelSender,
};
use learner::{
    Evolver,
    Learnable,
};
use plotters::prelude::*;

type Sender = ChannelSender<(Vec<(f64, f64)>, usize)>;
type Receiver = ChannelReceiver<(Vec<(f64, f64)>, usize)>;

fn true_func(x: f64) -> f64 {
    (2.0 * PI * x).sin()
}

#[derive(Clone)]
struct SineChecker {
    x: f64,
    guesses: Vec<(f64, f64)>,
    chan: Sender,
}

impl SineChecker {
    fn new() -> (Self, Receiver) {
        let (sender, receiver) = bounded(0);
        (
            Self {
                x: 0.0,
                guesses: Vec::new(),
                chan: sender,
            },
            receiver,
        )
    }
}

impl Debug for SineChecker {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "X: {}\nGuesses: {:?}", self.x, self.guesses)
    }
}

impl Learnable for SineChecker {
    type Error = String;

    fn get_input_data(&self) -> Vec<f64> {
        vec![self.x]
    }

    fn give_guess(&mut self, guess: (usize, f64)) -> Result<(), Self::Error> {
        self.guesses.push((self.x, guess.1));
        self.x += 0.005;

        match self.x {
            x if x > 1.0 => Err("finished".to_owned()),
            _ => Ok(()),
        }
    }

    fn get_points(&self) -> i32 {
        let results: Vec<f64> = self
            .guesses
            .iter()
            .copied()
            .map(|(x, y)| (true_func(x) - y).powi(2))
            .collect();

        -((results.iter().fold(0.0, |acc, y| acc + y) / (results.len() as f64) * 10000.0).round()
            as i32)
    }

    fn reset(&mut self) {
        self.x = 0.0;
        self.guesses = Vec::new();
    }

    fn print(&self, current_generation: usize) -> String {
        self.chan
            .send((self.guesses.clone(), current_generation))
            .unwrap();

        "written frame to graph.gif".to_string()
    }
}

fn draw(chan: Receiver) {
    let root = BitMapBackend::gif("graph.gif", (960, 720), 50)
        .unwrap()
        .into_drawing_area();

    while let Ok((guessed, generation)) = chan.recv() {
        root.fill(&WHITE).unwrap();
        let root = root.margin(10, 10, 10, 10);

        let mut chart = ChartBuilder::on(&root)
            .caption(
                "Real vs Guessed",
                ("sans-serif", 30).into_font(),
            )
            .x_label_area_size(20)
            .y_label_area_size(40)
            .build_cartesian_2d(0f32..1f32, -1.5f32..1.5f32)
            .unwrap();

        chart
            .configure_mesh()
            .x_labels(5)
            .y_labels(5)
            .draw()
            .unwrap();

        chart
            .draw_series(LineSeries::new(
                (0..500)
                    .map(|x| x as f64 / 500.0)
                    .map(|x| (x as f32, true_func(x) as f32)),
                &BLUE,
            ))
            .unwrap()
            .label("sin(2 * pi * x)")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &RED));

        chart
            .draw_series(PointSeries::of_element(
                guessed.into_iter().map(|(x, y)| (x as f32, y as f32)),
                3,
                &RED,
                &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.stroke_width(1)),
            ))
            .unwrap()
            .label(format!("Generation: {}", generation))
            .legend(|(x, y)| Circle::new((x + 11, y), 3, RED.stroke_width(1)));

        chart
            .configure_series_labels()
            .background_style(&WHITE.mix(0.8))
            .border_style(&BLACK)
            .label_font(("sans-serif", 15).into_font())
            .position(SeriesLabelPosition::UpperRight)
            .draw()
            .unwrap();

        root.present().unwrap();
    }
}

fn main() {
    let (sine_checker, chan) = SineChecker::new();

    let mut evolver = Evolver::builder()
        .set_to_learn(sine_checker)
        .set_organism_count(250)
        .set_layer_dimensions(vec![1, 16, 16, 16, 1])
        .set_print_to_shell(false)
        .build();

    thread::spawn(|| draw(chan));

    evolver.run_n_generations(250);
}
