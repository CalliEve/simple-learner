use std::{fs, fmt::Write};

use learner::{
    Data,
    MultipleLinearRegression,
};

fn main() {
    let data = vec![
        vec![40.0, 8.0, 10.0],
        vec![65.0, 9.0, 11.0],
        vec![73.0, 11.0, 11.0],
        vec![84.0, 10.0, 12.0],
        vec![90.0, 12.0, 13.0],
        vec![101.0, 14.0, 16.0],
        vec![164.0, 17.0, 19.0],
        vec![119.0, 15.0, 17.0],
        vec![143.0, 19.0, 20.0],
        vec![102.0, 15.0, 25.0],
        vec![147.0, 19.0, 26.0],
        vec![165.0, 21.0, 28.0],
        vec![180.0, 22.0, 29.0],
        vec![208.0, 22.0, 34.0],
        vec![206.0, 24.0, 34.0],
        vec![223.0, 26.0, 37.0],
        vec![295.0, 28.0, 45.0],
        vec![248.0, 29.0, 45.0],
        vec![249.0, 32.0, 50.0],
        vec![278.0, 31.0, 55.0],
        vec![297.0, 33.0, 60.0],
        vec![362.0, 39.0, 70.0],
        vec![338.0, 37.0, 65.0],
        vec![354.0, 38.0, 66.0],
        vec![376.0, 39.0, 70.0],
        vec![387.0, 40.0, 75.0],
        vec![399.0, 42.0, 80.0],
        vec![429.0, 45.0, 90.0],
        vec![431.0, 46.0, 102.0],
        vec![450.0, 48.0, 109.0],
    ];

    let mut table: Data = data
        .clone()
        .into_iter()
        .collect();
    table.set_columns(vec!["X".to_owned(), "Y".to_owned(), "Z".to_owned()]);

    let linear_regression = MultipleLinearRegression::new(table, "Y").unwrap();

    let mut out = String::new();
    let mut total_error = 0f64;
    for row in &data {
        let predicted = linear_regression.guess_float(&[row[0], row[2]]).unwrap();
        write!(&mut out, "x = {}, y = {} gives z = {}, predicted: {}\n", row[0], row[2], row[1], predicted).unwrap();
        total_error += predicted - row[1]
    }
    write!(&mut out, "total error: {:.3}\ncorrelation coefficient: {:.3}", total_error, linear_regression.get_correlation_coefficient()).unwrap();

    println!("{}", &out);

    fs::write("prediction.txt", out).unwrap();
}

