use std::num::ParseIntError;

#[derive(Clone, Debug)]
pub enum GameError {
    LaneFull,
    BoardFull,
    IndexError,
    InputError(ParseIntError),
    SystemError,
}

impl From<ParseIntError> for GameError {
    fn from(e: ParseIntError) -> Self {
        Self::InputError(e)
    }
}
