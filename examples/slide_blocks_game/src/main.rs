mod board;
mod errors;
mod field;

use std::io::stdin;

use learner::Evolver;

pub use board::Board;
pub use errors::GameError;

fn main() {
    // play a game yourself
    // game::run_manual_game()

    // Creating the game to run the neural network against
    let mut to_learn = Board::new();
    to_learn.set_print_interval(0);

    // Creating the neural network
    let mut evolver = Evolver::builder()
        .set_to_learn(to_learn)
        .set_thread_count(16)
        .set_organism_count(500)
        .set_layer_dimensions(vec![36, 40, 40, 40, 5])
        .build();

    // Run the neural network for 10k generations
    evolver.run_n_generations(10000);
}

#[allow(dead_code)]
pub fn run_manual_game() -> Result<(), GameError> {
    let mut board = Board::new();
    println!("{:?}", board);

    loop {
        println!("please provide the lane to add to");
        let mut input = String::new();
        let res = stdin().read_line(&mut input);
        if res.is_err() {
            return Err(GameError::SystemError);
        }

        let lane = input.trim().parse::<usize>()?;

        if lane > 4 {
            println!("only values 0 up to and including 4 are allowed");
            continue;
        }

        print!("\x1B[2J");

        board.add_new(lane)?;
    }
}
