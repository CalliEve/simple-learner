use std::fmt;
use std::fmt::Debug;

use learner::Learnable;
use rand::prelude::*;

use super::{
    field::Field,
    GameError,
};

const SHOOT_BLOCKS: [u32; 6] = [2, 4, 8, 16, 32, 64];

#[derive(Clone)]
pub struct Board {
    pub fields: Vec<Vec<Field>>,
    pub next: u32,
    round: u32,
    pub rng: StdRng,
    print_interval: u8,
    pub points: u32,
    pub moves: u32,
}

impl Debug for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Move: {}\nPoints: {}\n-------------------------------
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|
|{}|{}|{}|{}|{}|\n
  next block: {}",
            self.moves + 1,
            self.points,
            self.fields[0][0],
            self.fields[1][0],
            self.fields[2][0],
            self.fields[3][0],
            self.fields[4][0],
            self.fields[0][1],
            self.fields[1][1],
            self.fields[2][1],
            self.fields[3][1],
            self.fields[4][1],
            self.fields[0][2],
            self.fields[1][2],
            self.fields[2][2],
            self.fields[3][2],
            self.fields[4][2],
            self.fields[0][3],
            self.fields[1][3],
            self.fields[2][3],
            self.fields[3][3],
            self.fields[4][3],
            self.fields[0][4],
            self.fields[1][4],
            self.fields[2][4],
            self.fields[3][4],
            self.fields[4][4],
            self.fields[0][5],
            self.fields[1][5],
            self.fields[2][5],
            self.fields[3][5],
            self.fields[4][5],
            self.fields[0][6],
            self.fields[1][6],
            self.fields[2][6],
            self.fields[3][6],
            self.fields[4][6],
            self.next
        )
    }
}

impl Board {
    pub fn new() -> Board {
        let mut b = Board {
            fields: vec![vec![Field::default(); 8]; 5],
            next: 2,
            rng: StdRng::from_rng(thread_rng()).unwrap(),
            points: 0,
            print_interval: 1,
            round: 0,
            moves: 0,
        };

        b.ready_next();

        b
    }

    pub fn set_print_interval(&mut self, interval: u8) {
        self.print_interval = interval;
    }

    fn ready_next(&mut self) {
        // self.next = *SHOOT_BLOCKS[..].choose(&mut self.rng).unwrap();
        self.next = SHOOT_BLOCKS[(self.round % 6) as usize]
    }

    pub fn add_new(&mut self, lane_nr: usize) -> Result<(), GameError> {
        let mut full = true;
        for i in 0..5 {
            if !self.fields[i][6].is_set() {
                full = false;
                break;
            }
        }

        let lane_full = self.fields[lane_nr][6].is_set()
            && (self.fields[lane_nr][6].has_value() != self.next
                || self.fields[lane_nr][7].is_set());

        if full && lane_full {
            return Err(GameError::BoardFull);
        } else if lane_full {
            return Err(GameError::LaneFull);
        }

        let lane: &mut Vec<Field> = &mut self.fields[lane_nr as usize];

        self.moves += 1;

        let mut y: u8 = 0;
        for f in lane {
            if !f.is_set() {
                f.set_value(self.next);
                break;
            }
            y += 1;
        }

        self.ready_next();

        self.merge_surroundings((lane_nr as u8, y))?;

        if self.print_interval != 0 && self.round % self.print_interval as u32 == 0 {
            println!("{:?}", self);
        }
        self.round += 1;

        Ok(())
    }

    fn merge_surroundings(&mut self, newest: (u8, u8)) -> Result<(), GameError> {
        let mut merge_times: u8 = 0;
        let block_value = self.get_field(newest)?.has_value();

        if newest.0 != 0 {
            let left = self.get_mut_field((newest.0 - 1, newest.1))?;
            if left.has_value() == block_value {
                merge_times += 1;
                left.empty()
            }
        }

        if newest.0 != 4 {
            let right = self.get_mut_field((newest.0 + 1, newest.1))?;
            if right.has_value() == block_value {
                merge_times += 1;
                right.empty()
            }
        }

        if newest.1 != 0 {
            let lower = self.get_mut_field((newest.0, newest.1 - 1))?;
            if lower.has_value() == block_value {
                merge_times += 1;
                lower.empty()
            }
        }

        let change_block = self.get_mut_field(newest)?;

        match merge_times {
            1 => {
                change_block.set_value(block_value * 2);
                self.points += block_value * 2;
            },
            2 => {
                change_block.set_value(block_value * 4);
                self.points += block_value * 2 + block_value * 4;
            },
            3 => {
                change_block.set_value(block_value * 8);
                self.points += block_value * 2 + block_value * 4 + block_value * 8;
            },
            _ => return Ok(()),
        }

        self.merge_surroundings(newest)?;

        self.move_down()
    }

    fn move_down(&mut self) -> Result<(), GameError> {
        for x in 0..5 {
            for y in 0..7 {
                if !self.fields[x][y].is_set() && self.fields[x][y + 1].is_set() {
                    let old_val = self.fields[x][y + 1].has_value();

                    self.fields[x][y].set_value(old_val);
                    self.fields[x][y + 1].empty();

                    self.merge_surroundings((x as u8, y as u8))?;
                }
            }
        }

        Ok(())
    }

    fn get_mut_field(&mut self, field: (u8, u8)) -> Result<&mut Field, GameError> {
        if field.0 > 6 {
            return Err(GameError::IndexError);
        }

        let lane = &mut self.fields[field.0 as usize];

        if lane.len() <= field.1 as usize {
            return Err(GameError::IndexError);
        }

        Ok(&mut lane[field.1 as usize])
    }

    fn get_field(&self, field: (u8, u8)) -> Result<&Field, GameError> {
        if field.0 > 6 {
            return Err(GameError::IndexError);
        }

        let lane = &self.fields[field.0 as usize];

        if lane.len() <= field.1 as usize {
            return Err(GameError::IndexError);
        }

        Ok(&lane[field.1 as usize])
    }
}

impl Learnable for Board {
    type Error = GameError;

    fn reset(&mut self) {
        self.fields = vec![vec![Field::default(); 8]; 5];
        self.points = 0;
        self.round = 0;
        self.moves = 0;
        self.next = 2;
    }

    fn give_guess(&mut self, guess: (usize, f64)) -> Result<(), Self::Error> {
        self.add_new(guess.0)
    }

    fn get_input_data(&self) -> Vec<f64> {
        let mut res: Vec<f64> = self
            .fields
            .iter()
            .map(|l| &l[..7])
            .flatten()
            .map(|f| f.has_value() as f64)
            .collect();
        res.push(self.next as f64);

        res
    }

    fn get_points(&self) -> i32 {
        self.points as i32
    }
}
