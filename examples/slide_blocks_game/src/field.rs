use std::default::Default;
use std::fmt;
use std::fmt::Display;

#[derive(Clone, Debug)]
pub struct Field {
    value: u32,
    is_set: bool,
}

impl Default for Field {
    fn default() -> Field {
        Self {
            value: 0,
            is_set: false,
        }
    }
}

impl Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.is_set() {
            let mut res = format!("{}", self.value);

            if res.len() < 5 {
                res = " ".to_owned() + &res;
                while res.len() < 5 {
                    res += " "
                }
            }

            write!(f, "{}", res)
        } else {
            write!(f, "     ")
        }
    }
}

impl Field {
    pub fn is_set(&self) -> bool {
        self.is_set
    }

    pub fn has_value(&self) -> u32 {
        self.value
    }

    pub fn set_value(&mut self, value: u32) {
        self.value = value;
        self.is_set = true;
    }

    pub fn empty(&mut self) {
        self.value = 0;
        self.is_set = false;
    }
}

impl PartialEq for Field {
    fn eq(&self, other: &Self) -> bool {
        self.is_set && other.is_set && self.value == other.value
    }
}
