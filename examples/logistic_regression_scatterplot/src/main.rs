use learner::{
    Data,
    DataValue,
    LogisticRegression,
};
use plotters::prelude::*;

fn main() {
    let mut data: Data = vec![
        (0.40, 0),
        (0.65, 0),
        (0.73, 0),
        (0.84, 0),
        (0.90, 0),
        (1.01, 0),
        (1.64, 0),
        (1.19, 0),
        (1.43, 0),
        (1.02, 0),
        (1.47, 0),
        (1.65, 1),
        (1.80, 0),
        (2.08, 0),
        (2.06, 1),
        (2.23, 0),
        (2.95, 1),
        (2.48, 1),
        (2.49, 1),
        (2.78, 1),
        (2.97, 1),
        (3.62, 1),
        (3.38, 1),
        (3.54, 1),
        (3.76, 1),
        (3.87, 1),
        (3.99, 1),
        (4.29, 1),
        (4.31, 1),
        (4.50, 1),
        (463.0, 1),
        (478.0, 1),
    ]
    .into_iter()
    .map(|(x, y)| vec![DataValue::from(x as f64), DataValue::from(y as i32)])
    .collect();

    data.set_columns(vec!["X".to_owned(), "Y".to_owned()]);

    let mut logistic_regression = LogisticRegression::new(data.clone(), "Y").unwrap();

    logistic_regression.fit(5);

    draw(data, logistic_regression);
}

fn draw(data: Data, logistic_regression: LogisticRegression) {
    let root = BitMapBackend::new("graph.png", (960, 720)).into_drawing_area();

    root.fill(&WHITE).unwrap();
    let root = root.margin(10, 10, 10, 10);

    let mut chart = ChartBuilder::on(&root)
        .caption(
            format!("Logistic Regression"),
            ("sans-serif", 30).into_font(),
        )
        .x_label_area_size(20)
        .y_label_area_size(40)
        .build_cartesian_2d(0f32..5f32, (-0.5_f32)..1.5_f32)
        .unwrap();

    chart
        .configure_mesh()
        .x_labels(5)
        .y_labels(5)
        .draw()
        .unwrap();

    chart
        .draw_series(PointSeries::of_element(
            data.iter_rows().map(|p| {
                (
                    p[0].as_float().unwrap() as f32,
                    p[1].as_float().unwrap() as f32,
                )
            }),
            4,
            &BLACK,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Truth Values")
        .legend(|(x, y)| Circle::new((x + 9, y), 4, BLACK.filled()));

    chart
        .draw_series(LineSeries::new(
            (0_i32..500_i32).map(|f| {
                (
                    f as f32 / 100_f32,
                    logistic_regression
                        .guess(&[(f / 100).into()])
                        .unwrap()
                        .as_float()
                        .unwrap() as f32,
                )
            }),
            &BLUE,
        ))
        .unwrap()
        .label("Regression Line")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &BLUE));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .label_font(("sans-serif", 15).into_font())
        .position(SeriesLabelPosition::UpperLeft)
        .draw()
        .unwrap();

    root.present().unwrap();
}
