use learner::SimpleLinearRegression;
use plotters::prelude::*;

fn main() {
    let data = vec![
        (40.0, 8.0),
        (65.0, 9.0),
        (73.0, 11.0),
        (84.0, 10.0),
        (90.0, 7.0),
        (101.0, 14.0),
        (164.0, 17.0),
        (119.0, 15.0),
        (143.0, 19.0),
        (102.0, 15.0),
        (147.0, 19.0),
        (165.0, 21.0),
        (180.0, 22.0),
        (208.0, 22.0),
        (206.0, 24.0),
        (223.0, 26.0),
        (295.0, 28.0),
        (248.0, 29.0),
        (249.0, 32.0),
        (278.0, 34.0),
        (297.0, 35.0),
        (362.0, 39.0),
        (338.0, 37.0),
        (354.0, 38.0),
        (376.0, 39.0),
        (387.0, 40.0),
        (399.0, 42.0),
        (429.0, 45.0),
        (431.0, 46.0),
        (450.0, 47.0),
    ];

    let linear_regression = SimpleLinearRegression::from_floats(&data);

    draw(data, linear_regression);
}

fn draw(data: Vec<(f64, f64)>, linear_regression: SimpleLinearRegression) {
    let root = BitMapBackend::new("graph.png", (1500, 1000)).into_drawing_area();

    root.fill(&WHITE).unwrap();
    let root = root.margin(10, 10, 10, 10);

    let mut chart = ChartBuilder::on(&root)
        .caption(format!("Linear Regression, r = {:.3}", linear_regression.get_correlation_coefficient()), ("sans-serif", 30).into_font())
        .x_label_area_size(20)
        .y_label_area_size(40)
        .build_cartesian_2d(0f32..470f32, 0f32..50f32)
        .unwrap();

    chart
        .configure_mesh()
        .x_labels(5)
        .y_labels(5)
        .draw()
        .unwrap();

    chart
        .draw_series(PointSeries::of_element(
            data.iter().map(|p| (p.0 as f32, p.1 as f32)),
            3,
            &BLACK,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Data Points")
        .legend(|(x, y)| Circle::new((x + 11, y), 3, BLACK.filled()));

    chart
        .draw_series(LineSeries::new(
            (0..4700).map(|f| (f as f32 / 10.0, linear_regression.guess(f as f64/ 10.0) as f32)),
            &BLUE,
        ))
        .unwrap()
        .label("Regression Line")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &BLUE));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .position(SeriesLabelPosition::UpperLeft)
        .draw()
        .unwrap();

    root.present().unwrap();
}
