use std::fs;

use learner::{DecisionTree, Data, DataValue};

fn main() {
    let mut data: Data = Data::new(ndarray::Array2::from_shape_vec((8, 4), vec![
        (true, 30.0, "cloudy", false),
        (false, 30.0, "cloudy", true),
        (true, 10.0, "sunny", false),
        (false, 10.0, "sunny", false),
        (false, 20.0, "sunny", true),
        (false, 30.0, "sunny", true),
        (false, 20.0, "cloudy", false),
        (true, 30.0, "sunny", false),
    ].into_iter().map(|(r, t, c, g)| vec![r.into(), (t as f64).into(), c.into(), g.into()]).flatten().collect::<Vec<DataValue>>()).unwrap());
    data.set_columns(vec!["Rainy".to_owned(), "Temperature".to_owned(), "Cloud Cover".to_owned(), "Good Weather".to_owned()]);

    let mut tree = DecisionTree::new();

    println!("Starting training");
    tree.train(data, "Good Weather").unwrap();
    println!("Finished training");

    let prediction = tree.predict(&[false.into(), 28.0.into(), "cloudy".into()]).unwrap();
    println!("Predicted: {}", prediction);
    assert_eq!(prediction, true);

    let prediction = tree.predict(&[true.into(), 25.0.into(), "sunny".into()]).unwrap();
    println!("Predicted: {}", prediction);
    assert_eq!(prediction, false);
    
    fs::write("tree.txt", tree.to_string()).unwrap();
    println!("{}", tree)
}

