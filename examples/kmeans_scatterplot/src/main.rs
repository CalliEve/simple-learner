use learner::{Data, DataValue, KMeans};
use plotters::prelude::*;

fn main() {
    let mut data: Data = vec![
        vec![196, 31],
        vec![65, 20],
        vec![265, 26],
        vec![84, 36],
        vec![188, 7],
        vec![23, 23],
        vec![184, 35],
        vec![119, 15],
        vec![233, 18],
        vec![55, 27],
        vec![180, 8],
        vec![208, 32],
        vec![259, 14],
        vec![208, 22],
        vec![113, 3],
        vec![439, 13],
        vec![389, 11],
        vec![222, 22],
        vec![230, 21],
        vec![192, 20],
        vec![258, 16],
        vec![461, 25],
        vec![132, 24],
        vec![98, 31],
        vec![212, 32],
        vec![197, 26],
        vec![466, 20],
        vec![320, 30],
        vec![401, 21],
        vec![412, 18],
    ].into_iter().collect();

    data.set_columns(vec!["X".to_owned(), "Y".to_owned()]);

    let mut kmeans = KMeans::new(data, &["X", "Y"], 3).unwrap();

    kmeans.iterate_till_done();

    draw(&kmeans.get_points(), &kmeans.get_cluster_centers());
}

fn draw(data: &[&[Vec<DataValue>]], points: &[&[DataValue]]) {
    let root = BitMapBackend::new("graph.png", (960, 720)).into_drawing_area();

    root.fill(&WHITE).unwrap();
    let root = root.margin(10, 10, 10, 10);

    let mut chart = ChartBuilder::on(&root)
        .caption("KMeans Clustering", ("sans-serif", 30).into_font())
        .x_label_area_size(20)
        .y_label_area_size(40)
        .build_cartesian_2d(0f32..500f32, 0f32..40f32)
        .unwrap();

    chart
        .configure_mesh()
        .x_labels(5)
        .y_labels(5)
        .draw()
        .unwrap();

    chart
        .draw_series(PointSeries::of_element(
            data[0].iter().map(|p| (p[0].as_float().unwrap() as f32, p[1].as_float().unwrap() as f32)),
            3,
            &RED,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Cluster 1")
        .legend(|(x, y)| Circle::new((x + 11, y), 3, RED.filled()));

    chart
        .draw_series(PointSeries::of_element(
            data[1].iter().map(|p| (p[0].as_float().unwrap() as f32, p[1].as_float().unwrap() as f32)),
            3,
            &BLUE,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Cluster 2")
        .legend(|(x, y)| Circle::new((x + 11, y), 3, BLUE.filled()));

    chart
        .draw_series(PointSeries::of_element(
            data[2].iter().map(|p| (p[0].as_float().unwrap() as f32, p[1].as_float().unwrap() as f32)),
            3,
            &GREEN,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Cluster 3")
        .legend(|(x, y)| Circle::new((x + 11, y), 3, GREEN.filled()));

    chart
        .draw_series(PointSeries::of_element(
            points.iter().map(|p| (p[0].as_float().unwrap() as f32, p[1].as_float().unwrap() as f32)),
            5,
            &BLACK,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Cluster Centers")
        .legend(|(x, y)| Circle::new((x + 10, y), 5, BLACK.filled()));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .label_font(("sans-serif", 15).into_font())
        .position(SeriesLabelPosition::UpperRight)
        .draw()
        .unwrap();

    root.present().unwrap();
}
