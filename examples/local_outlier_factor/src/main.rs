use learner::{
    Data,
    LocalOutlierFactor,
};
use plotters::prelude::*;

fn main() {
    let mut data: Data = vec![
        vec![23, 3],
        vec![65, 20],
        vec![75, 24],
        vec![84, 26],
        vec![98, 21],
        vec![113, 13],
        vec![119, 15],
        vec![132, 24],
        vec![180, 18],
        vec![184, 25],
        vec![188, 17],
        vec![192, 20],
        vec![196, 21],
        vec![197, 26],
        vec![201, 21],
        vec![202, 20],
        vec![208, 25],
        vec![209, 22],
        vec![209, 28],
        vec![212, 18],
        vec![213, 22],
        vec![222, 22],
        vec![230, 21],
        vec![233, 18],
        vec![239, 13],
        vec![258, 16],
        vec![259, 14],
        vec![265, 26],
        vec![276, 20],
        vec![461, 25],
    ]
    .into_iter()
    .collect();

    data.set_columns(vec!["X".to_owned(), "Y".to_owned()]);

    let mut model = LocalOutlierFactor::new(5);

    model
        .fit(data.clone(), &["X", "Y"])
        .unwrap();

    draw(data, model);
}

fn draw(mut data: Data, mut model: LocalOutlierFactor) {
    let outliers = model
        .get_outliers(2.0)
        .unwrap();
    data.iter_mut().for_each(|v| *v = v.as_float_value().unwrap());

    let root = BitMapBackend::new("graph.png", (960, 720)).into_drawing_area();

    root.fill(&WHITE)
        .unwrap();
    let root = root.margin(10, 10, 10, 10);

    let mut chart = ChartBuilder::on(&root)
        .caption("Local Outlier Factor", ("sans-serif", 30).into_font())
        .x_label_area_size(20)
        .y_label_area_size(40)
        .build_cartesian_2d(0f32..500f32, 0f32..30f32)
        .unwrap();

    chart
        .configure_mesh()
        .x_labels(5)
        .y_labels(5)
        .draw()
        .unwrap();

    chart
        .draw_series(PointSeries::of_element(
            data.iter_rows()
                .filter(|r| !outliers.contains(&r.to_vec()))
                .map(|r| {
                    (
                        r[0].as_float()
                            .unwrap() as f32,
                        r[1].as_float()
                            .unwrap() as f32,
                    )
                }),
            3,
            &BLUE,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Normal data")
        .legend(move |(x, y)| Circle::new((x + 11, y), 3, BLUE.filled()));

    chart
        .draw_series(PointSeries::of_element(
            outliers.into_iter()
                .map(|r| {
                    (
                        r[0].as_float()
                            .unwrap() as f32,
                        r[1].as_float()
                            .unwrap() as f32,
                    )
                }),
            3,
            &RED,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Outliers")
        .legend(move |(x, y)| Circle::new((x + 11, y), 3, RED.filled()));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .label_font(("sans-serif", 15).into_font())
        .position(SeriesLabelPosition::UpperRight)
        .draw()
        .unwrap();

    root.present()
        .unwrap();
}
