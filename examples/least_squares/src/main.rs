use learner::{LeastSquares, Data};
use plotters::prelude::*;
use rand::prelude::*;

fn main() {
    let mut rng = thread_rng();
    let data: Vec<Vec<f64>> = (20..=200).into_iter().map(|x| x as f64).map(|x| {
        let y = -0.000038 * x.powi(4) + 0.01 * x.powi(3) - 0.5f64 * x.powi(2) + 123f64 * x + rng.gen_range(-30f64..50f64) * x + 5000f64;
        vec![x, y]
    }).collect();
    
    let mut table: Data = data
        .clone()
        .into_iter()
        .collect();
    table.set_columns(vec!["X".to_owned(), "Y".to_owned()]);
    
    let mut least_squares = LeastSquares::new();
    least_squares.set_max_degree(8);

    least_squares.fit(table.get_column("X").unwrap().1, table.get_column("Y").unwrap().1).unwrap();

    draw(data, least_squares);
}


fn draw(data: Vec<Vec<f64>>, least_squares: LeastSquares) {
    let root = BitMapBackend::new("graph.png", (1500, 1000)).into_drawing_area();

    root.fill(&WHITE).unwrap();
    let root = root.margin(10, 10, 10, 10);

    let mut chart = ChartBuilder::on(&root)
        .caption("Least Squares", ("sans-serif", 30).into_font())
        .x_label_area_size(20)
        .y_label_area_size(40)
        .build_cartesian_2d(15f32..210f32, 5000f32..42000f32)
        .unwrap();

    chart
        .configure_mesh()
        .x_labels(5)
        .y_labels(5)
        .draw()
        .unwrap();

    chart
        .draw_series(PointSeries::of_element(
            data.iter().map(|p| (p[0] as f32, p[1] as f32)),
            3,
            &BLACK,
            &|c, s, st| EmptyElement::at(c) + Circle::new((0, 0), s, st.filled()),
        ))
        .unwrap()
        .label("Data Points")
        .legend(|(x, y)| Circle::new((x + 11, y), 3, BLACK.filled()));

    chart
        .draw_series(LineSeries::new(
            (210..2000).map(|f| (f as f32 / 10.0, least_squares.guess_float(f as f64/ 10.0).unwrap() as f32)),
            &BLUE,
        ))
        .unwrap()
        .label("Regression Line")
        .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], &BLUE));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .position(SeriesLabelPosition::UpperLeft)
        .draw()
        .unwrap();

    root.present().unwrap();
}
